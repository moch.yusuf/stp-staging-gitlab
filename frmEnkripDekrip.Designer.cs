﻿namespace TCPClientServer
{
    partial class frmEnkripDekrip
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtfInput = new System.Windows.Forms.RichTextBox();
            this.rtfOutput = new System.Windows.Forms.RichTextBox();
            this.btnEnkrip = new System.Windows.Forms.Button();
            this.btnDekrip = new System.Windows.Forms.Button();
            this.rdAS400_Staging = new System.Windows.Forms.RadioButton();
            this.rdStaging_BI = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkEncryptedYN = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // rtfInput
            // 
            this.rtfInput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtfInput.BackColor = System.Drawing.Color.Black;
            this.rtfInput.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtfInput.ForeColor = System.Drawing.Color.Lime;
            this.rtfInput.Location = new System.Drawing.Point(14, 40);
            this.rtfInput.Name = "rtfInput";
            this.rtfInput.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtfInput.Size = new System.Drawing.Size(445, 443);
            this.rtfInput.TabIndex = 0;
            this.rtfInput.Text = "";
            // 
            // rtfOutput
            // 
            this.rtfOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtfOutput.BackColor = System.Drawing.Color.Black;
            this.rtfOutput.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtfOutput.ForeColor = System.Drawing.Color.Lime;
            this.rtfOutput.Location = new System.Drawing.Point(646, 37);
            this.rtfOutput.Name = "rtfOutput";
            this.rtfOutput.ReadOnly = true;
            this.rtfOutput.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtfOutput.Size = new System.Drawing.Size(445, 443);
            this.rtfOutput.TabIndex = 1;
            this.rtfOutput.Text = "";
            // 
            // btnEnkrip
            // 
            this.btnEnkrip.BackColor = System.Drawing.Color.Black;
            this.btnEnkrip.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnkrip.ForeColor = System.Drawing.Color.White;
            this.btnEnkrip.Location = new System.Drawing.Point(500, 227);
            this.btnEnkrip.Name = "btnEnkrip";
            this.btnEnkrip.Size = new System.Drawing.Size(87, 25);
            this.btnEnkrip.TabIndex = 2;
            this.btnEnkrip.Text = "Enkrip";
            this.btnEnkrip.UseVisualStyleBackColor = false;
            this.btnEnkrip.Click += new System.EventHandler(this.btnEnkrip_Click);
            // 
            // btnDekrip
            // 
            this.btnDekrip.BackColor = System.Drawing.Color.Black;
            this.btnDekrip.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDekrip.ForeColor = System.Drawing.Color.White;
            this.btnDekrip.Location = new System.Drawing.Point(500, 258);
            this.btnDekrip.Name = "btnDekrip";
            this.btnDekrip.Size = new System.Drawing.Size(87, 25);
            this.btnDekrip.TabIndex = 3;
            this.btnDekrip.Text = "Dekrip";
            this.btnDekrip.UseVisualStyleBackColor = false;
            this.btnDekrip.Click += new System.EventHandler(this.btnDekrip_Click);
            // 
            // rdAS400_Staging
            // 
            this.rdAS400_Staging.AutoSize = true;
            this.rdAS400_Staging.Checked = true;
            this.rdAS400_Staging.Location = new System.Drawing.Point(7, 20);
            this.rdAS400_Staging.Name = "rdAS400_Staging";
            this.rdAS400_Staging.Size = new System.Drawing.Size(130, 18);
            this.rdAS400_Staging.TabIndex = 7;
            this.rdAS400_Staging.TabStop = true;
            this.rdAS400_Staging.Text = "AS400 - Staging";
            this.rdAS400_Staging.UseVisualStyleBackColor = true;
            // 
            // rdStaging_BI
            // 
            this.rdStaging_BI.AutoSize = true;
            this.rdStaging_BI.Location = new System.Drawing.Point(7, 45);
            this.rdStaging_BI.Name = "rdStaging_BI";
            this.rdStaging_BI.Size = new System.Drawing.Size(137, 18);
            this.rdStaging_BI.TabIndex = 8;
            this.rdStaging_BI.Text = "Staging - BI SPK";
            this.rdStaging_BI.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdAS400_Staging);
            this.groupBox1.Controls.Add(this.rdStaging_BI);
            this.groupBox1.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(467, 116);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(173, 70);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pointing";
            // 
            // chkEncryptedYN
            // 
            this.chkEncryptedYN.AutoSize = true;
            this.chkEncryptedYN.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkEncryptedYN.ForeColor = System.Drawing.Color.White;
            this.chkEncryptedYN.Location = new System.Drawing.Point(500, 42);
            this.chkEncryptedYN.Name = "chkEncryptedYN";
            this.chkEncryptedYN.Size = new System.Drawing.Size(82, 18);
            this.chkEncryptedYN.TabIndex = 11;
            this.chkEncryptedYN.Text = "Enkripsi";
            this.chkEncryptedYN.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(216, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 14);
            this.label1.TabIndex = 12;
            this.label1.Text = "Input";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(841, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 14);
            this.label2.TabIndex = 13;
            this.label2.Text = "Output";
            // 
            // frmEnkripDekrip
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1103, 493);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkEncryptedYN);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnDekrip);
            this.Controls.Add(this.btnEnkrip);
            this.Controls.Add(this.rtfOutput);
            this.Controls.Add(this.rtfInput);
            this.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmEnkripDekrip";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dev Tools";
            this.Load += new System.EventHandler(this.frmEnkripDekrip_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtfInput;
        private System.Windows.Forms.RichTextBox rtfOutput;
        private System.Windows.Forms.Button btnEnkrip;
        private System.Windows.Forms.Button btnDekrip;
        private System.Windows.Forms.RadioButton rdAS400_Staging;
        private System.Windows.Forms.RadioButton rdStaging_BI;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkEncryptedYN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}