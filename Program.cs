﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using TCPClientServer.BI_SPK;
using TCPClientServer.General;
using TCPClientServer.Model;
using TCPClientServer.Services;

namespace TCPClientServer
{
    static public class Program
    {
        private static frmServer formserver;
        private static Socket clientSocket;
        private static EnkDek enkdek = new EnkDek();

        private static GeneralModel gm = new GeneralModel();
        private static MessageProcessing mp = new MessageProcessing();
        private static Outgoing outgoing = new Outgoing();
        private static Incoming incoming = new Incoming();
        private static Parameter parameter = new Parameter();
        private static Status status = new Status();

        public static TPKWebServiceClient _wsdl = new TPKWebServiceClient();

        public static string data = "";
        public static IPHostEntry ipHostInfo;
        public static IPAddress ipAddr;
        public static IPEndPoint localEndPoint;
        public static Socket listener;
        public static Thread listeningThread;
        public static Thread alertThread;
        private static bool stopMe = false;
        private static bool foundEcho = false;

        static System.Threading.Timer theTimer;

        private static object locker = new object();

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            gm.TCPPort = mp.GetTcpPort.TCPPort;
            gm.MaxMsgLength = mp.GetMaxMsgLength.MaxMsgLength;
            gm.ThreadSleep = mp.GetThreadSleep.ThreadSleep;

            gm.RecordCount_getDaftarPesertaWilayah = mp.GetDaftarPesertaWilayah_RecCount.RecordCount_getDaftarPesertaWilayah;
            gm.RecordCount_getDKEKreditInwardIndividual = mp.GetDKEKreditInwardIndividual_RecCount.RecordCount_getDKEKreditInwardIndividual;

            gm.Dummy_Path_Incoming = mp.GetDummyPathIncoming.Dummy_Path_Incoming;
            gm.Dummy_Path_Outgoing = mp.GetDummyPathOutgoing.Dummy_Path_Outgoing;
            gm.Dummy_Path_Parameter = mp.GetDummyPathParameter.Dummy_Path_Parameter;
            gm.Log_Path = mp.GetLogPath.Log_Path;
            gm.KeepLogInDays = mp.GetKeepLogInDays.KeepLogInDays;
            gm.StagnantHandler = mp.GetStagnantChecking.StagnantHandler;

            ipHostInfo = Dns.Resolve(Dns.GetHostName());
            //ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            //IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            if (ipHostInfo.AddressList.Length > 1)
            {
                ipAddr = ipHostInfo.AddressList[1];
            }
            else
            {
                ipAddr = ipHostInfo.AddressList[0];
            }
            localEndPoint = new IPEndPoint(ipAddr, gm.TCPPort);

            // Creation TCP/IP Socket using 
            // Socket Class Costructor 
            listener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            listener.Bind(localEndPoint);

            //listener.Listen(10);

            if (Environment.UserInteractive)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                formserver = new frmServer();
                formserver.Load += OnFormLoad;
                formserver.FormClosed += OnFormClose;
                formserver.Resize += OnFormResize;

                gm.IsEncBISPK = formserver.chkEncBISPK.Checked;
                gm.IsEncAS400 = formserver.chkEncAS400.Checked;

                gm.IsDummy_Incoming = formserver.chkDummyIncoming.Checked;
                gm.IsDummy_Outgoing = formserver.chkDummyOutgoing.Checked;
                gm.IsDummy_Parameter = formserver.chkDummyParameter.Checked;

                gm.IsWriteLogToFile = formserver.chkLogFile.Checked;

                listener.Listen(10);

                Application.Run(formserver);
            }
            else
            {
                gm.IsEncBISPK = mp.GetIsEncryptBISPK.IsEncBISPK;
                gm.IsEncAS400 = mp.GetIsEncryptAS400.IsEncAS400;

                gm.IsDummy_Parameter = mp.GetIsDummyParameter.IsDummy_Parameter;
                gm.IsDummy_Incoming = mp.GetIsDummyIncoming.IsDummy_Incoming;
                gm.IsDummy_Outgoing = mp.GetIsDummyOutgoing.IsDummy_Outgoing;

                gm.IsWriteLogToFile = mp.GetIsWriteLogToFile.IsWriteLogToFile;

                listener.Listen(10);

                ServiceBase.Run(new STPStagingService());
            }

            //listeningThread = new Thread(StartServer);
            //listeningThread.IsBackground = true;
            //listeningThread.Start();

            //theTimer = new System.Threading.Timer(StartServer, null, 1000, 2000);
        }

        static internal void OnStart(string[] args)
        {
            listeningThread = new Thread(StartServer);
            listeningThread.IsBackground = true;
            listeningThread.Start();

            //alertThread = new Thread(checkLastLog);
            //alertThread.IsBackground = true;
            //alertThread.Start();

            //int iStagnant = (gm.StagnantHandler * 60) * 1000;
            //theTimer = new System.Threading.Timer(checkLastLog, null, iStagnant, iStagnant);
            //theTimer = new System.Threading.Timer(checkLastLog, null, 5000, 10000);            
        }

        private static void OnFormLoad(object sender, EventArgs e)
        {
            //StartServer(null);
            OnStart(null);
        }

        private static void OnFormClose(object sender, EventArgs e)
        {
            OnStop();
        }

        private static void OnFormResize(object sender, EventArgs e)
        {
            if (formserver.WindowState == FormWindowState.Minimized)
            {
                formserver.Hide();
                formserver.notifyIcon1.Visible = true;
            }
        }

        static internal void OnStop()
        {
            try
            {
                if (listeningThread.IsAlive)
                {
                    stopMe = false;
                    foundEcho = false;

                    //theTimer.Change(0, Timeout.Infinite);
                    //theTimer.Dispose();

                    //listeningThread.Abort();
                }

                if (clientSocket != null)
                {
                    clientSocket.Shutdown(SocketShutdown.Both);
                    clientSocket.Close();
                }

                Thread.Sleep(10000);
            }
            catch (Exception e)
            {

            }
        }

        static internal void RestartService()
        {
            ServiceController sc = new ServiceController("STP Staging Service");
            //ServiceController sc = new ServiceController("service gui");
            var status = sc.Status;

            if (status == ServiceControllerStatus.Running)
            {
                sc.Stop();
                Thread.Sleep(3000);
            }
            sc.Start();
        }

        private static void StartServer(object state)
        {
            while (!stopMe)
            {
                try
                {
                    writeLog("Waiting connection ...");

                    // Suspend while waiting for 
                    // incoming connection Using 
                    // Accept() method the server 
                    // will accept connection of client 
                    clientSocket = listener.Accept();

                    int msg_length = 0;
                    int msg_full_length = 0;

                    //add new DSR
                    int LengthMessage, IndexMessage = 0;
                    string FixData = "";
                    //end DSR

                    string part_of_data = "";

                    while (!stopMe)
                    {
                        try
                        {
                            if (data == "")
                            {
                                writeLog("Incoming connection from: " + clientSocket.RemoteEndPoint);
                            }

                            // Data buffer 
                            Byte[] bytes = new Byte[1024];

                            int numByte = clientSocket.Receive(bytes);

                            if (numByte == 0)
                            {
                                writeLog(clientSocket.RemoteEndPoint + " disconnected...");
                            }

                            if (data == "" && numByte > 0)
                            {
                                data = Encoding.ASCII.GetString(bytes, 0, numByte);
                                msg_full_length = int.Parse(data.Substring(0, 6));
                                msg_length = data.Length;
                            }
                            else
                            {
                                part_of_data = Encoding.ASCII.GetString(bytes, 0, numByte); ;
                                data += part_of_data;
                            }

                            if (data == "")
                            {
                                break;
                            }

                            msg_length += part_of_data.Length;

                            if (msg_full_length == msg_length)
                            {
                                writeLog(data);

                                DataProcessing(data);

                                data = "";
                                part_of_data = "";
                                msg_length = 0;
                                msg_full_length = 0;
                            }
                            //new DSR begin
                            else if (msg_full_length < msg_length)
                            {
                                int cekL = 0;

                                do
                                {
                                    if (data.Length < 6)
                                    {
                                        cekL = 1;
                                        break;
                                    }

                                    if (data.Length >= 6)
                                    {
                                        LengthMessage = Convert.ToInt32(data.Substring(IndexMessage, 6));
                                        
                                        if (data.Length >= LengthMessage)
                                        {
                                            FixData = data.Substring(IndexMessage, LengthMessage);
                                            IndexMessage = IndexMessage + FixData.Length;

                                            //writeLog(FixData);

                                            DataProcessing(FixData);
                                            data = data.Remove(0, IndexMessage);

                                            IndexMessage = 0;
                                        }
                                        else
                                        {
                                            cekL = 1;
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        cekL = 1;
                                        break;
                                    }
                                } while (IndexMessage < data.Length);

                                if (cekL == 0)
                                {
                                    data = "";
                                    part_of_data = "";
                                    msg_length = 0;
                                    msg_full_length = 0;
                                    LengthMessage = 0;
                                    IndexMessage = 0;
                                    FixData = "";
                                }
                            }
                            //end DSR
                        }
                        catch (Exception se)
                        {
                            if (Environment.UserInteractive)
                            {
                                formserver.rtfLog.Invoke((MethodInvoker)(() => formserver.rtfLog.AppendText(DateTime.Now + "> ERROR: " + se.Message + "\n")));
                            }
                            else
                            {
                                writeLog("Exception1: " + se.Message + "\n");
                                writeLog("Restarting Service...");
                                //RestartService();
                                OnStop();
                                if (listeningThread.IsAlive)
                                {
                                    listeningThread.Abort();
                                }
                                OnStart(null);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    //Console.WriteLine(e.ToString());
                    if (Environment.UserInteractive)
                    {
                        formserver.rtfLog.Invoke((MethodInvoker)(() => formserver.rtfLog.AppendText(DateTime.Now + "> ERROR: " + e.Message + "\n")));
                    }
                    else
                    {
                        writeLog("Exception2: " + e.Message + "\n");
                        writeLog("Restarting Service...");
                        //RestartService();
                        OnStop();
                        if (listeningThread.IsAlive)
                        {
                            listeningThread.Abort();
                        }
                        OnStart(null);
                    }
                }
            }
        }

        private static void DataProcessing(string data)
        {
            try
            {
                //set reply
                string reply = "";
                int replyLength = 0;

                //if (data.Length > 16)
                if (data.Length >= 48)
                {
                    gm.msg_length = data.Substring(0, 6);
                    gm.msg_type = data.Substring(6, 3);
                    gm.trans_code = data.Substring(9, 3);
                    gm.msg_seq = data.Substring(12, 4);
                    gm.bic = data.Substring(16, 8);
                    gm.authcode = data.Substring(24, 24).Trim();

                    if (data.Length >= 48)
                    {
                        gm.sAuthCodeBI = enkdek.DekripAS400(gm.authcode);
                        gm.dke = data.Substring(48);

                        //parameter
                        //if (data.Length > 48)
                        if (data.Length >= 54)
                        {
                            //gm.dke = data.Substring(48);
                            gm.dke = data.Substring(54);

                            gm.sDkeBI = enkdek.DekripAS400(gm.dke);

                            //if (formserver.chkEncBISPK.Checked)
                            if (gm.IsEncBISPK)
                            {
                                gm.sDkeBI = enkdek.EnkripBISPK(gm.sDkeBI);
                            }
                            else
                            {
                                gm.sDkeBI = enkdek.StringToBase64(gm.sDkeBI);
                            }

                            //if uncheck as400 encrypted
                            //if (formserver.chkEncAS400.Checked == false)
                            if (gm.IsEncAS400 == false)
                            {
                                gm.authcode = enkdek.DekripAS400(gm.authcode);
                                gm.dke = enkdek.DekripAS400(gm.dke);
                            }
                        }

                        gm.bDKE = string.IsNullOrEmpty(gm.sDkeBI) ? null : Convert.FromBase64String(gm.sDkeBI);
                    }
                }
                else //echo
                {
                    if (data.Length >= 9)
                    {
                        gm.msg_type = data.Substring(6, 3);
                    }
                }

                switch (gm.msg_type)
                {
                    //message balikan: message_length(6digit)+message
                    //echo test
                    case "800":

                        reply = "800000SUCCESS";
                        replyLength = reply.Length + 6;
                        reply = ("000000" + replyLength).Right(6);
                        reply = reply + "800000SUCCESS";

                        sendReply(reply);

                        ////test multiple msg to tcp
                        //iThreadSleep = MessageProcessing.GetThreadSleep.ThreadSleep;
                        //for (int i = 0; i <= 10; i++)
                        //{
                        //    sendReply(reply+i);
                        //    Thread.Sleep(iThreadSleep);
                        //}

                        break;
                    //Outgoing
                    case "101":
                        gm.sBatchId = data.Substring(48, 6);
                        gm.batch_id_to_tcp = gm.sBatchId;

                        reply = outgoing.SendDKEKreditIndividual(gm);

                        if (reply != null)
                        {
                            sendReply(reply);
                        }

                        break;

                    //Incoming
                    case "202":
                        gm.benefid = data.Substring(48, 8);
                        if (gm.benefid.Trim() != "")
                            gm.sBenefId = enkdek.DekripAS400(gm.benefid);
                        else
                            gm.sBenefId = "";
                        gm.periode = data.Substring(60, 1);
                        gm.sPeriode = gm.periode;

                        gm.sandikota = data.Substring(56, 4);
                        if (gm.sandikota.Trim() != "")
                            gm.sSandiKota = enkdek.DekripAS400(gm.sandikota);
                        else
                            gm.sSandiKota = "";
                        gm.ReqSequence = data.Substring(61, 6);
                        reply = incoming.getDKEKreditInwardIndividual(gm);

                        if (reply != null)
                        {
                            writeLog(reply);
                        }

                        break;

                    //get Parameter
                    case "303":

                        reply = parameter.getParameter(gm);

                        if (reply != null)
                        {
                            writeLog(reply);
                        }
                        break;

                    //get status
                    case "404":

                        ///validasi error index
                        if (data.Length > 48)
                        {
                            gm.sBatchId = data.Substring(48, 6);
                        }

                        gm.batch_id_to_tcp = gm.sBatchId;

                        reply = status.getStatus(gm);

                        if (reply != null)
                        {
                            sendReply(reply);
                        }

                        break;
                }
            }
            catch (Exception e)
            {
                writeLog("Exception3: " + e.Message.ToString());
                writeLog("Restarting Service...");
                OnStop();
                //writeLog("Service restart has been successful");
                if (listeningThread.IsAlive)
                {
                    listeningThread.Abort();
                }
                OnStart(null);
            }
        }

        public static void sendReply(string reply)
        {
            byte[] rpl = Encoding.ASCII.GetBytes(reply);
            clientSocket.Send(rpl, rpl.Length, SocketFlags.None);
            writeLog("Reply: " + reply);
        }

        public static void writeLog(string msg)
        {
            DateTime today = DateTime.Now; // As DateTime
            string s_now = today.ToString("hh:mm:ss");
            string s_today = today.ToString("yyyy-MM-dd"); // As String
            string s_fullpath = gm.Log_Path + s_today + "\\" + "Log_" + s_today + ".txt";

            clearLog(gm.KeepLogInDays);

            if (Environment.UserInteractive)
            {
                formserver.rtfLog.Invoke((MethodInvoker)(() => formserver.rtfLog.AppendText(s_now + "> " + msg + "\n")));
            }

            if (gm.IsWriteLogToFile)
            {
                try
                {
                    if (!Directory.Exists(gm.Log_Path + "\\" + s_today))
                    {
                        Directory.CreateDirectory(gm.Log_Path + "\\" + s_today);

                        lock (locker)
                        {
                            File.WriteAllBytes(s_fullpath, Encoding.ASCII.GetBytes(s_now + "> " + msg + Environment.NewLine));
                        }
                    }
                    else
                    {
                        lock (locker)
                        {
                            File.AppendAllText(s_fullpath, s_now + "> " + msg + Environment.NewLine);
                        }
                    }
                }
                catch (Exception e)
                {
                    writeLog("Exception: method:writeLog() " + e.Message.ToString());
                }
            }
        }

        public static void clearLog(int days)
        {
            try
            {
                DateTime today = DateTime.Now; // As DateTime                
                DateTime dayMin = today.Date.AddDays(-days);

                var dirs = Directory.GetDirectories(gm.Log_Path);

                foreach (var dir in dirs)
                {
                    if (DateTime.ParseExact(dir.Right(10), "yyyy-MM-dd", CultureInfo.CurrentCulture) <= dayMin)
                    {
                        if (Directory.Exists(dir))
                        {
                            Directory.Delete(dir, true);
                        }
                    }
                };
            }
            catch (Exception e)
            {
                writeLog("Exception: method:clearLog() " + e.Message.ToString());
            }
        }

        public static void checkLastLog(object state)
        {
            try
            {
                if (clientSocket != null)
                {
                    DateTime today = DateTime.Now; // As DateTime         
                    string s_today = today.ToString("yyyy-MM-dd"); // As String       

                    var file = Directory.GetFiles(gm.Log_Path + s_today);

                    FileInfo fi = new FileInfo(file[0]);
                    long fSize = fi.Length;

                    IEnumerable<string> lastLine;
                    lock (locker)
                    {
                        lastLine = File.ReadLines(file[0]);
                    }

                    List<string> listLine = new List<string>();

                    foreach (var last in lastLine)
                    {
                        listLine.Add(last);
                    }

                    int listLineLength = listLine.ToArray().Length;
                    int cnt = listLineLength - 15;
                    while (cnt < listLineLength)
                    {
                        if (listLine[cnt].Length == 26)
                        {
                            if (listLine[cnt].Substring(10, 16) == "000016800000ECHO")
                            {
                                foundEcho = true;
                                //theTimer.Change(600000, 600000);
                                break;
                            }
                        }
                        cnt++;
                    }

                    if (foundEcho == false)
                    {
                        writeLog("ALERT001: Echo dari AS400 belum diterima");

                        writeLog("Restarting Service...");
                        OnStop();
                        //writeLog("Service restart has been successful");
                        OnStart(null);
                    }
                }
            }
            catch (Exception e)
            {
                writeLog("Exception: method:checkLastLog() " + e.Message.ToString());
                writeLog("Restarting Service...");
                OnStop();
                //writeLog("Service restart has been successful");
                OnStart(null);
            }
        }
    }

    static class Extensions
    {
        /// <summary>
        /// Get substring of specified number of characters on the right.
        /// </summary>
        public static string Right(this string value, int length)
        {
            try
            {
                return value.Substring(value.Length - length);
            }
            catch (Exception e)
            {
                return null;
                Program.writeLog($"Class: Extensions {Environment.NewLine} Function: Right() {Environment.NewLine} ErrorMessage: {e.Message}");
            }
        }

        public static string Left(this string value, int length)
        {
            try
            {
                return value.Substring(0, length);
            }
            catch (Exception e)
            {
                return null;
                Program.writeLog($"Class: Extensions {Environment.NewLine} Function: Left() {Environment.NewLine} ErrorMessage: {e.Message}");
            }
        }
    }
}
