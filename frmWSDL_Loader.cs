﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Reflection;
using System.Web.Services.Description;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using TCPClientServer.BI_SPK;
using TCPClientServer.General;
using TCPClientServer.WsdlLib;

namespace TCPClientServer
{
    public partial class frmWSDL_Loader : Form
    {
        //public event TreeNodeMouseClickEventHandler NodeMouseDoubleClick;
        public event TreeNodeMouseClickEventHandler NodeMouseClick;

        //public TreeNode tNode;

        public XmlSchemas _schemas;
        public ServiceDescriptionCollection _services = new ServiceDescriptionCollection();

        private Point dragStartLocation;

        private TPKWebServiceClient _wsdl = new TPKWebServiceClient();
        //private string Wsdl_Url = null;

        private EnkDek enkdek = new EnkDek();

        private byte[] result;
        private string sResult;

        private MessageProcessing messageProcessing = new MessageProcessing();

        public frmWSDL_Loader()
        {
            InitializeComponent();

            //treeViewWSDL.MouseDown += new MouseEventHandler(treeViewWSDL_MouseDown);
            //treeViewWSDL.MouseUp += new MouseEventHandler(treeViewWSDL_MouseUp);
            //treeViewWSDL.MouseMove += new MouseEventHandler(treeViewWSDL_MouseMove);

            //treeViewWSDL.NodeMouseClick += new TreeNodeMouseClickEventHandler(treeViewWSDL_NodeMouseClick);
            tvwService.NodeMouseDoubleClick += new TreeNodeMouseClickEventHandler(treeViewWSDL_NodeMouseDoubleClick);

            txtBIC.Text = "BBIJIDJA";
            txtAuthcode.Text = "TesSTP2019";

            cboURL.Text = messageProcessing.GetWsdlURL.WsdlUrl;
        }


        //void treeViewWSDL_MouseLeave(object sender, EventArgs e)
        //{
        //    dragStartLocation = Point.Empty;
        //}

        //void treeViewWSDL_MouseMove(object sender, MouseEventArgs e)
        //{
        //    if (!dragStartLocation.IsEmpty)
        //    {

        //        int n = 10;
        //        int deltaX = e.X - dragStartLocation.X;
        //        int deltaY = e.Y - dragStartLocation.Y;

        //        if (deltaX > n || deltaX < n || deltaY > n || deltaY < n)
        //        {
        //            treeViewWSDL.Cursor = Cursors.SizeAll;


        //            treeViewWSDL.Location = new Point(treeViewWSDL.Location.X + deltaX, treeViewWSDL.Location.Y + deltaY);
        //            this.Text = "treeView1 moved to: " + treeViewWSDL.Location.ToString();
        //        }
        //    }
        //}

        void treeViewWSDL_MouseDown(object sender, MouseEventArgs e)
        {
            this.tvwService.SelectedNode = this.tvwService.GetNodeAt(e.X, e.Y);
        }

        void treeViewWSDL_MouseUp(object sender, MouseEventArgs e)
        {
            this.tvwService.SelectedNode = this.tvwService.GetNodeAt(e.X, e.Y);
        }

        //void treeViewWSDL_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        //{
        //    if (dragStartLocation == Point.Empty || dragStartLocation == e.Location)
        //    {
        //        dragStartLocation = Point.Empty;
        //        this.Text = "treeViewWSDL_NodeMouseClick - " + e.Node.Text;
        //    }
        //}

        void treeViewWSDL_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeNode tn = tvwService.SelectedNode;
            //this.txtDKE.Text = "";
            if (tn != null)
            {
                if (tn.Level == 3)
                {
                    txtRequest.Text = tvwService.SelectedNode.Text;
                }

                //if (tn.Tag is string)
                //{
                //    string message = (string)tn.Tag;
                //    this.txtDKE.Text = message;
                //}
                //    else
                //        if (tn.Tag is SchemaParser.NodeData)
                //    {
                //        SchemaParser.NodeData data = tn.Tag as SchemaParser.NodeData;
                //        string message = data.baseObj.ToString() + "\n";
                //        message += data.Namespace + "\n";
                //        message += data.Type + "\n";
                //        string xml = XMLGenerator.GetXmlSample(tn);

                //        message += xml;

                //        this.txtDKE.Text = message;
                //    }
            }
        }


        private void btnLoadWsdl_Click(object sender, EventArgs e)
        {
            //AppSettingsReader settingsReader = new AppSettingsReader();
            //string WSDL_Url = (string)settingsReader.GetValue("WSDL_Url", typeof(string));

            //LoadWebService(WSDL_Url);
            WsdlLoader(cboURL.Text);
        }

        //public void TreeViewGenerator(List<MethodInfo> list)
        //{

        //ServiceNode = tvwWSDL.Nodes.Add("BI SPK");
        //ServiceNode.Collapse();

        //foreach (var a in list)
        //{
        //    tvwWSDL.Nodes[0].Nodes.Add(a.Name).Nodes.Add("Request");
        //}
        //}

        public void WsdlLoader(string uri)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                XmlTextReader reader = new XmlTextReader(uri);
                ServiceDescription service = ServiceDescription.Read(reader);
                WSDLParser parser = new WSDLParser(service);

                this.tvwService.Nodes.Add(parser.ServiceNode);
                this.cboURL.Items.Add(cboURL.Text);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString());
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void treeViewWSDL_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                switch (txtRequest.Text)
                {
                    case "getDaftarAlasanPenolakan":
                        result = _wsdl.getDaftarAlasanPenolakan(txtBIC.Text, txtAuthcode.Text);
                        break;
                    case "getDaftarKota":
                        result = _wsdl.getDaftarKota(txtBIC.Text, txtAuthcode.Text);
                        break;
                    case "getDaftarPeserta":
                        result = _wsdl.getDaftarPeserta(txtBIC.Text, txtAuthcode.Text);
                        break;
                    case "getDaftarPesertaWilayah":
                        result = _wsdl.getDaftarPesertaWilayah(txtBIC.Text, txtAuthcode.Text);
                        break;
                    case "getDaftarPropinsi":
                        result = _wsdl.getDaftarPropinsi(txtBIC.Text, txtAuthcode.Text);
                        break;
                    case "getDaftarSandiTransaksi":
                        result = _wsdl.getDaftarSandiTransaksi(txtBIC.Text, txtAuthcode.Text);
                        break;
                    case "getDaftarWilayahKliring":
                        result = _wsdl.getDaftarWilayahKliring(txtBIC.Text, txtAuthcode.Text);
                        break;
                    case "getDaftarWilayahKota":
                        result = _wsdl.getDaftarWilayahKota(txtBIC.Text, txtAuthcode.Text);
                        break;
                    case "getDKEKreditInwardBulk":
                        result = _wsdl.getDKEKreditInwardBulk(txtBIC.Text, txtAuthcode.Text, txtBenefBIC.Text, txtPeriode.Text, txtSandiKota.Text);
                        break;
                    case "getDKEKreditInwardIndividual":
                        result = _wsdl.getDKEKreditInwardIndividual(txtBIC.Text, txtAuthcode.Text, txtBenefBIC.Text, txtPeriode.Text, txtSandiKota.Text);
                        break;
                    case "getDKEKreditOutwardSSKBulk":
                        result = _wsdl.getDKEKreditOutwardSSKBulk(txtBIC.Text, txtAuthcode.Text, txtSenderBIC.Text);
                        break;
                    case "getDKEKreditOutwardSSKIndividual":
                        result = _wsdl.getDKEKreditOutwardSSKIndividual(txtBIC.Text, txtAuthcode.Text, txtSenderBIC.Text);
                        break;
                    case "getDKEPengembalianInwardBulk":
                        result = _wsdl.getDKEPengembalianInwardBulk(txtBIC.Text, txtAuthcode.Text, txtBenefBIC.Text);
                        break;
                    case "getDKEPengembalianInwardIndividual":
                        result = _wsdl.getDKEPengembalianInwardIndividual(txtBIC.Text, txtAuthcode.Text, txtBenefBIC.Text, txtSandiZona.Text, txtSandiWilayahKliring.Text);
                        break;
                    case "getDKEPengembalianOutwardSSKBulk":
                        result = _wsdl.getDKEPengembalianOutwardSSKBulk(txtBIC.Text, txtAuthcode.Text, txtSenderBIC.Text);
                        break;
                    case "getDKEPengembalianOutwardSSKIndividual":
                        result = _wsdl.getDKEPengembalianOutwardSSKIndividual(txtBIC.Text, txtAuthcode.Text, txtSenderBIC.Text);
                        break;
                    case "getDKEPenyerahanInwardBulk":
                        result = _wsdl.getDKEPenyerahanInwardBulk(txtBIC.Text, txtAuthcode.Text, txtBenefBIC.Text);
                        break;
                    case "getDKEPenyerahanInwardIndividual":
                        result = _wsdl.getDKEPenyerahanInwardIndividual(txtBIC.Text, txtAuthcode.Text, txtBenefBIC.Text, txtSandiZona.Text, txtSandiWilayahKliring.Text);
                        break;
                    case "getDKEPenyerahanOutwardSSKBulk":
                        result = _wsdl.getDKEPenyerahanOutwardSSKBulk(txtBIC.Text, txtAuthcode.Text, txtSenderBIC.Text);
                        break;
                    case "getDKEPenyerahanOutwardSSKIndividual":
                        result = _wsdl.getDKEPenyerahanOutwardSSKIndividual(txtBIC.Text, txtAuthcode.Text, txtSenderBIC.Text);
                        break;
                    case "getStatusDKEKreditOutwardBulk":
                        result = _wsdl.getStatusDKEKreditOutwardBulk(txtBIC.Text, txtAuthcode.Text, txtBatchId.Text);
                        break;
                    case "getStatusDKEKreditOutwardIndividual":
                        result = _wsdl.getStatusDKEKreditOutwardIndividual(txtBIC.Text, txtAuthcode.Text, txtBatchId.Text);
                        break;
                    case "getStatusDKEPengembalianOutwardBulk":
                        result = _wsdl.getStatusDKEPengembalianOutwardBulk(txtBIC.Text, txtAuthcode.Text, txtBatchId.Text);
                        break;
                    case "getStatusDKEPengembalianOutwardIndividual":
                        result = _wsdl.getStatusDKEPengembalianOutwardIndividual(txtBIC.Text, txtAuthcode.Text, txtBatchId.Text);
                        break;
                    case "getStatusDKEPenyerahanOutwardBulk":
                        result = _wsdl.getStatusDKEPenyerahanOutwardBulk(txtBIC.Text, txtAuthcode.Text, txtBatchId.Text);
                        break;
                    case "getStatusDKEPenyerahanOutwardIndividual":
                        result = _wsdl.getStatusDKEPenyerahanOutwardIndividual(txtBIC.Text, txtAuthcode.Text, txtBatchId.Text);
                        break;
                    case "sendDKEKreditBulk":
                        result = _wsdl.sendDKEKreditBulk(txtBIC.Text, txtAuthcode.Text, Convert.FromBase64String(txtDKE.Text));
                        break;
                    case "sendDKEKreditIndividual":
                        result = _wsdl.sendDKEKreditIndividual(txtBIC.Text, txtAuthcode.Text, Convert.FromBase64String(txtDKE.Text));
                        break;
                    case "sendDKEPengembalianBulk":
                        result = _wsdl.sendDKEPengembalianBulk(txtBIC.Text, txtAuthcode.Text, Convert.FromBase64String(txtDKE.Text));
                        break;
                    case "sendDKEPengembalianIndividual":
                        result = _wsdl.sendDKEPengembalianIndividual(txtBIC.Text, txtAuthcode.Text, Convert.FromBase64String(txtDKE.Text));
                        break;
                    case "sendDKEPenyerahanBulk":
                        result = _wsdl.sendDKEPenyerahanBulk(txtBIC.Text, txtAuthcode.Text, Convert.FromBase64String(txtDKE.Text));
                        break;
                    case "sendDKEPenyerahanIndividual":
                        result = _wsdl.sendDKEPenyerahanIndividual(txtBIC.Text, txtAuthcode.Text, Convert.FromBase64String(txtDKE.Text));
                        break;                    
                }

                if (result != null)
                {
                    sResult = enkdek.StringToBase64(result);

                    rtfResponse.Text = sResult;
                    //rtfResponse.Text += "\n\n" + enkdek.DekripBISPK(sResult);

                }
                else
                {
                    rtfResponse.Text = "No Result or method not found in wsdl!";
                }
            }
            catch (Exception ex)
            {
                rtfResponse.Text = ex.Message;
            }
        }

        private void btnEnkrip_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(txtDKE.Text))
                {
                    txtDKE.Text = enkdek.EnkripBISPK(txtDKE.Text);

                    btnEnkripDKE.Enabled = false;
                    btnDekripDKE.Enabled = true;
                    btnDekripDKE.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDekripDKE_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(txtDKE.Text))
                {
                    txtDKE.Text = enkdek.DekripBISPK(txtDKE.Text);

                    btnDekripDKE.Enabled = false;
                    btnEnkripDKE.Enabled = true;
                    btnEnkripDKE.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDekripResponse_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(rtfResponse.Text))
                {
                    var respon = enkdek.DekripBISPK(rtfResponse.Text);
                    rtfResponse.Clear();
                    rtfResponse.Text = respon;
                    rtfResponse.Refresh();

                    btnDekripResponse.Enabled = false;
                    btnEnkripResponse.Enabled = true;
                    btnEnkripResponse.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEnkripResponse_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(rtfResponse.Text))
                {
                    var respon = enkdek.EnkripBISPK(rtfResponse.Text);
                    rtfResponse.Clear();
                    rtfResponse.Text = respon;
                    rtfResponse.Refresh();

                    btnEnkripResponse.Enabled = false;
                    btnDekripResponse.Enabled = true;
                    btnDekripResponse.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

  
    }
}
