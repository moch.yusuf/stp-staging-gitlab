﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TCPClientServer.Model;

namespace TCPClientServer.General
{
    public class MessageProcessing
    {
        private GeneralModel generalModel = new GeneralModel();

        public GeneralModel GetTcpPort
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                generalModel.TCPPort = int.Parse(ConfigurationManager.AppSettings["TCPPort"]);

                return generalModel;
            }
        }

        public GeneralModel GetMaxMsgLength
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                generalModel.MaxMsgLength = int.Parse(ConfigurationManager.AppSettings["MaxMsgLength"]);

                return generalModel;
            }
        }

        public GeneralModel GetWsdlURL
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                generalModel.WsdlUrl = (string)ConfigurationManager.AppSettings["WSDL_Url"];

                return generalModel;
            }
        }

        public GeneralModel GetThreadSleep
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                generalModel.ThreadSleep = int.Parse(ConfigurationManager.AppSettings["ThreadSleep"]);

                return generalModel;
            }
        }

        //Length Setting
        //public GeneralModel GetDaftarPesertaWilayah_RecSize
        //{
        //    get
        //    {
        //        ConfigurationManager.RefreshSection("appSettings");
        //        generalModel.RecordSize_getDaftarPesertaWilayah = int.Parse(ConfigurationManager.AppSettings["RecordSize_getDaftarPesertaWilayah"]);

        //        return generalModel;
        //    }
        //}

        public GeneralModel GetDaftarPesertaWilayah_RecCount
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                generalModel.RecordCount_getDaftarPesertaWilayah = int.Parse(ConfigurationManager.AppSettings["RecordCount_getDaftarPesertaWilayah"]);

                return generalModel;
            }
        }

        //public GeneralModel GetDKEKreditInwardIndividual_RecSize
        //{
        //    get
        //    {
        //        ConfigurationManager.RefreshSection("appSettings");
        //        generalModel.RecordSize_getDKEKreditInwardIndividual = int.Parse(ConfigurationManager.AppSettings["RecordSize_getDKEKreditInwardIndividual"]);

        //        return generalModel;
        //    }
        //}

        public GeneralModel GetDKEKreditInwardIndividual_RecCount
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                generalModel.RecordCount_getDKEKreditInwardIndividual = int.Parse(ConfigurationManager.AppSettings["RecordCount_getDKEKreditInwardIndividual"]);

                return generalModel;
            }
        }

        public GeneralModel GetDummyPathIncoming
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                generalModel.Dummy_Path_Incoming = ConfigurationManager.AppSettings["DummyPathIncoming"];

                return generalModel;
            }
        }

        public GeneralModel GetDummyPathOutgoing
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                generalModel.Dummy_Path_Outgoing = ConfigurationManager.AppSettings["DummyPathOutgoing"];

                return generalModel;
            }
        }

        public GeneralModel GetDummyPathParameter
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                generalModel.Dummy_Path_Parameter = ConfigurationManager.AppSettings["DummyPathParameter"];

                return generalModel;
            }
        }

        public GeneralModel GetIsDummyOutgoing
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                generalModel.IsDummy_Outgoing = bool.Parse(ConfigurationManager.AppSettings["IsDummyOutgoing"]);

                return generalModel;
            }
        }

        public GeneralModel GetIsDummyIncoming
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                generalModel.IsDummy_Incoming = bool.Parse(ConfigurationManager.AppSettings["IsDummyIncoming"]);

                return generalModel;
            }
        }

        public GeneralModel GetIsDummyParameter
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                generalModel.IsDummy_Parameter = bool.Parse(ConfigurationManager.AppSettings["IsDummyParameter"]);

                return generalModel;
            }
        }

        public GeneralModel GetIsEncryptBISPK
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                generalModel.IsEncBISPK = bool.Parse(ConfigurationManager.AppSettings["IsEncryptBISPK"]);

                return generalModel;
            }
        }

        public GeneralModel GetIsEncryptAS400
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                generalModel.IsEncAS400 = bool.Parse(ConfigurationManager.AppSettings["IsEncryptAS400"]);

                return generalModel;
            }
        }

        public GeneralModel GetIsWriteLogToFile
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                generalModel.IsWriteLogToFile = bool.Parse(ConfigurationManager.AppSettings["IsWriteLogToFile"]);

                return generalModel;
            }
        }

        public GeneralModel GetLogPath
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                generalModel.Log_Path = ConfigurationManager.AppSettings["LogPath"];

                return generalModel;
            }
        }

        public GeneralModel GetKeepLogInDays
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                generalModel.KeepLogInDays = int.Parse(ConfigurationManager.AppSettings["KeepLogInDays"]);

                return generalModel;
            }
        }

        public GeneralModel GetStagnantChecking
        {
            get
            {
                ConfigurationManager.RefreshSection("appSettings");
                generalModel.StagnantHandler = int.Parse(ConfigurationManager.AppSettings["StagnantHandler"]);

                return generalModel;
            }
        }

        public List<string> GetPartialData(string reply, int record_count)
        {
            List<string> listResult = new List<string>();
            List<string> listTemp = new List<string>();

            String[] listDefault;
            String sTemp = "";

            var vInput = reply.Replace("\u001a", "");

            Char[] separator = { '\n' };
            listDefault = vInput.Split(separator);

            var temp = "";
            foreach (String list in listDefault)
            {
                if (!String.IsNullOrEmpty(list))
                {
                    switch (list.Substring(0, 2))
                    {
                        //header incoming
                        case "01":
                            temp = list.Insert(83, setSpace(792));
                            break;

                        //footer incoming
                        case "31":
                            temp = list.Insert(7, setSpace(868));
                            break;

                        //detail - all transaction
                        default:
                            temp = list;
                            break;
                    }

                    listTemp.Add(temp);
                }
            }


            double iTempLength = listTemp.ToArray().Count();
            double iRecCount = Math.Ceiling(iTempLength / record_count);

            int x = 0;
            for (int i = 0; i < iRecCount; i++)
            {
                sTemp = "";
                for (int j = x; j < iTempLength; j++)
                {
                    sTemp += listTemp.ElementAtOrDefault(j) + "\n";
                    if (j >= (x + (record_count - 1)))
                    {
                        x = j + 1;
                        break;
                    }
                }

                listResult.Add(sTemp);
            }

            return listResult;
        }

        private String setSpace(int count)
        {
            var vSpace = "";

            for (int i = 1; i <= count; i++)
            {
                vSpace += " ";
            }

            return vSpace;
        }
    }
}
