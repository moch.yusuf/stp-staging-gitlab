﻿namespace TCPClientServer
{
    partial class frmWSDL_Loader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWSDL_Loader));
            this.tvwService = new System.Windows.Forms.TreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnLoadWsdl = new System.Windows.Forms.Button();
            this.txtDKE = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSend = new System.Windows.Forms.Button();
            this.txtBIC = new System.Windows.Forms.TextBox();
            this.txtAuthcode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRequest = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnEnkripDKE = new System.Windows.Forms.Button();
            this.btnDekripResponse = new System.Windows.Forms.Button();
            this.btnDekripDKE = new System.Windows.Forms.Button();
            this.cboURL = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.rtfResponse = new System.Windows.Forms.RichTextBox();
            this.btnEnkripResponse = new System.Windows.Forms.Button();
            this.txtBenefBIC = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPeriode = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSandiKota = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSenderBIC = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSandiZona = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtSandiWilayahKliring = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtBatchId = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tvwService
            // 
            this.tvwService.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tvwService.BackColor = System.Drawing.Color.Black;
            this.tvwService.ForeColor = System.Drawing.Color.Lime;
            this.tvwService.ImageIndex = 0;
            this.tvwService.ImageList = this.imageList1;
            this.tvwService.Location = new System.Drawing.Point(14, 86);
            this.tvwService.Name = "tvwService";
            this.tvwService.SelectedImageIndex = 0;
            this.tvwService.Size = new System.Drawing.Size(390, 473);
            this.tvwService.TabIndex = 0;
            this.tvwService.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeViewWSDL_AfterSelect);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            this.imageList1.Images.SetKeyName(3, "");
            this.imageList1.Images.SetKeyName(4, "");
            this.imageList1.Images.SetKeyName(5, "");
            this.imageList1.Images.SetKeyName(6, "");
            this.imageList1.Images.SetKeyName(7, "");
            this.imageList1.Images.SetKeyName(8, "");
            this.imageList1.Images.SetKeyName(9, "");
            this.imageList1.Images.SetKeyName(10, "");
            this.imageList1.Images.SetKeyName(11, "");
            this.imageList1.Images.SetKeyName(12, "");
            this.imageList1.Images.SetKeyName(13, "");
            this.imageList1.Images.SetKeyName(14, "");
            this.imageList1.Images.SetKeyName(15, "");
            this.imageList1.Images.SetKeyName(16, "");
            // 
            // btnLoadWsdl
            // 
            this.btnLoadWsdl.BackColor = System.Drawing.Color.Black;
            this.btnLoadWsdl.ForeColor = System.Drawing.Color.White;
            this.btnLoadWsdl.Location = new System.Drawing.Point(14, 55);
            this.btnLoadWsdl.Name = "btnLoadWsdl";
            this.btnLoadWsdl.Size = new System.Drawing.Size(101, 25);
            this.btnLoadWsdl.TabIndex = 15;
            this.btnLoadWsdl.Text = "Load WSDL";
            this.btnLoadWsdl.UseVisualStyleBackColor = false;
            this.btnLoadWsdl.Click += new System.EventHandler(this.btnLoadWsdl_Click);
            // 
            // txtDKE
            // 
            this.txtDKE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDKE.BackColor = System.Drawing.Color.Black;
            this.txtDKE.ForeColor = System.Drawing.Color.Lime;
            this.txtDKE.Location = new System.Drawing.Point(413, 156);
            this.txtDKE.Multiline = true;
            this.txtDKE.Name = "txtDKE";
            this.txtDKE.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDKE.Size = new System.Drawing.Size(658, 127);
            this.txtDKE.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(410, 311);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 14);
            this.label1.TabIndex = 18;
            this.label1.Text = "Response";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(407, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 14);
            this.label2.TabIndex = 19;
            this.label2.Text = "Request";
            // 
            // btnSend
            // 
            this.btnSend.BackColor = System.Drawing.Color.Black;
            this.btnSend.ForeColor = System.Drawing.Color.White;
            this.btnSend.Location = new System.Drawing.Point(819, 289);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(121, 25);
            this.btnSend.TabIndex = 20;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = false;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // txtBIC
            // 
            this.txtBIC.BackColor = System.Drawing.Color.Black;
            this.txtBIC.ForeColor = System.Drawing.Color.Lime;
            this.txtBIC.Location = new System.Drawing.Point(713, 27);
            this.txtBIC.Name = "txtBIC";
            this.txtBIC.Size = new System.Drawing.Size(100, 20);
            this.txtBIC.TabIndex = 21;
            // 
            // txtAuthcode
            // 
            this.txtAuthcode.BackColor = System.Drawing.Color.Black;
            this.txtAuthcode.ForeColor = System.Drawing.Color.Lime;
            this.txtAuthcode.Location = new System.Drawing.Point(819, 27);
            this.txtAuthcode.Name = "txtAuthcode";
            this.txtAuthcode.Size = new System.Drawing.Size(249, 20);
            this.txtAuthcode.TabIndex = 22;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(816, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 14);
            this.label3.TabIndex = 23;
            this.label3.Text = "Authcode";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(710, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 14);
            this.label4.TabIndex = 24;
            this.label4.Text = "BIC";
            // 
            // txtRequest
            // 
            this.txtRequest.BackColor = System.Drawing.Color.Black;
            this.txtRequest.ForeColor = System.Drawing.Color.Lime;
            this.txtRequest.Location = new System.Drawing.Point(410, 27);
            this.txtRequest.Name = "txtRequest";
            this.txtRequest.Size = new System.Drawing.Size(297, 20);
            this.txtRequest.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(410, 139);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 14);
            this.label5.TabIndex = 26;
            this.label5.Text = "DKE";
            // 
            // btnEnkripDKE
            // 
            this.btnEnkripDKE.BackColor = System.Drawing.Color.Black;
            this.btnEnkripDKE.ForeColor = System.Drawing.Color.White;
            this.btnEnkripDKE.Location = new System.Drawing.Point(946, 289);
            this.btnEnkripDKE.Name = "btnEnkripDKE";
            this.btnEnkripDKE.Size = new System.Drawing.Size(58, 25);
            this.btnEnkripDKE.TabIndex = 27;
            this.btnEnkripDKE.Text = "Enkrip";
            this.btnEnkripDKE.UseVisualStyleBackColor = false;
            this.btnEnkripDKE.Click += new System.EventHandler(this.btnEnkrip_Click);
            // 
            // btnDekripResponse
            // 
            this.btnDekripResponse.BackColor = System.Drawing.Color.Black;
            this.btnDekripResponse.ForeColor = System.Drawing.Color.White;
            this.btnDekripResponse.Location = new System.Drawing.Point(1010, 565);
            this.btnDekripResponse.Name = "btnDekripResponse";
            this.btnDekripResponse.Size = new System.Drawing.Size(58, 25);
            this.btnDekripResponse.TabIndex = 28;
            this.btnDekripResponse.Text = "Dekrip";
            this.btnDekripResponse.UseVisualStyleBackColor = false;
            this.btnDekripResponse.Click += new System.EventHandler(this.btnDekripResponse_Click);
            // 
            // btnDekripDKE
            // 
            this.btnDekripDKE.BackColor = System.Drawing.Color.Black;
            this.btnDekripDKE.ForeColor = System.Drawing.Color.White;
            this.btnDekripDKE.Location = new System.Drawing.Point(1010, 289);
            this.btnDekripDKE.Name = "btnDekripDKE";
            this.btnDekripDKE.Size = new System.Drawing.Size(58, 25);
            this.btnDekripDKE.TabIndex = 29;
            this.btnDekripDKE.Text = "Dekrip";
            this.btnDekripDKE.UseVisualStyleBackColor = false;
            this.btnDekripDKE.Click += new System.EventHandler(this.btnDekripDKE_Click);
            // 
            // cboURL
            // 
            this.cboURL.BackColor = System.Drawing.Color.Black;
            this.cboURL.ForeColor = System.Drawing.Color.Lime;
            this.cboURL.FormattingEnabled = true;
            this.cboURL.Location = new System.Drawing.Point(14, 27);
            this.cboURL.Name = "cboURL";
            this.cboURL.Size = new System.Drawing.Size(390, 22);
            this.cboURL.TabIndex = 30;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(12, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 14);
            this.label6.TabIndex = 31;
            this.label6.Text = "Url";
            // 
            // rtfResponse
            // 
            this.rtfResponse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtfResponse.BackColor = System.Drawing.Color.Black;
            this.rtfResponse.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtfResponse.ForeColor = System.Drawing.Color.Lime;
            this.rtfResponse.Location = new System.Drawing.Point(407, 331);
            this.rtfResponse.Name = "rtfResponse";
            this.rtfResponse.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtfResponse.Size = new System.Drawing.Size(661, 228);
            this.rtfResponse.TabIndex = 32;
            this.rtfResponse.Text = "";
            // 
            // btnEnkripResponse
            // 
            this.btnEnkripResponse.BackColor = System.Drawing.Color.Black;
            this.btnEnkripResponse.ForeColor = System.Drawing.Color.White;
            this.btnEnkripResponse.Location = new System.Drawing.Point(946, 565);
            this.btnEnkripResponse.Name = "btnEnkripResponse";
            this.btnEnkripResponse.Size = new System.Drawing.Size(58, 25);
            this.btnEnkripResponse.TabIndex = 33;
            this.btnEnkripResponse.Text = "Enkrip";
            this.btnEnkripResponse.UseVisualStyleBackColor = false;
            this.btnEnkripResponse.Click += new System.EventHandler(this.btnEnkripResponse_Click);
            // 
            // txtBenefBIC
            // 
            this.txtBenefBIC.BackColor = System.Drawing.Color.Black;
            this.txtBenefBIC.ForeColor = System.Drawing.Color.Lime;
            this.txtBenefBIC.Location = new System.Drawing.Point(713, 72);
            this.txtBenefBIC.Name = "txtBenefBIC";
            this.txtBenefBIC.Size = new System.Drawing.Size(100, 20);
            this.txtBenefBIC.TabIndex = 34;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(710, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 14);
            this.label7.TabIndex = 35;
            this.label7.Text = "Benef BIC";
            // 
            // txtPeriode
            // 
            this.txtPeriode.BackColor = System.Drawing.Color.Black;
            this.txtPeriode.ForeColor = System.Drawing.Color.Lime;
            this.txtPeriode.Location = new System.Drawing.Point(819, 72);
            this.txtPeriode.Name = "txtPeriode";
            this.txtPeriode.Size = new System.Drawing.Size(121, 20);
            this.txtPeriode.TabIndex = 36;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(816, 55);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 14);
            this.label8.TabIndex = 37;
            this.label8.Text = "Periode";
            // 
            // txtSandiKota
            // 
            this.txtSandiKota.BackColor = System.Drawing.Color.Black;
            this.txtSandiKota.ForeColor = System.Drawing.Color.Lime;
            this.txtSandiKota.Location = new System.Drawing.Point(946, 72);
            this.txtSandiKota.Name = "txtSandiKota";
            this.txtSandiKota.Size = new System.Drawing.Size(122, 20);
            this.txtSandiKota.TabIndex = 38;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(943, 55);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 14);
            this.label9.TabIndex = 39;
            this.label9.Text = "Sandi Kota";
            // 
            // txtSenderBIC
            // 
            this.txtSenderBIC.BackColor = System.Drawing.Color.Black;
            this.txtSenderBIC.ForeColor = System.Drawing.Color.Lime;
            this.txtSenderBIC.Location = new System.Drawing.Point(607, 72);
            this.txtSenderBIC.Name = "txtSenderBIC";
            this.txtSenderBIC.Size = new System.Drawing.Size(100, 20);
            this.txtSenderBIC.TabIndex = 40;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(604, 55);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 14);
            this.label10.TabIndex = 41;
            this.label10.Text = "Sender BIC";
            // 
            // txtSandiZona
            // 
            this.txtSandiZona.BackColor = System.Drawing.Color.Black;
            this.txtSandiZona.ForeColor = System.Drawing.Color.Lime;
            this.txtSandiZona.Location = new System.Drawing.Point(713, 109);
            this.txtSandiZona.Name = "txtSandiZona";
            this.txtSandiZona.Size = new System.Drawing.Size(100, 20);
            this.txtSandiZona.TabIndex = 42;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(710, 95);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 14);
            this.label11.TabIndex = 43;
            this.label11.Text = "Sandi Zona";
            // 
            // txtSandiWilayahKliring
            // 
            this.txtSandiWilayahKliring.BackColor = System.Drawing.Color.Black;
            this.txtSandiWilayahKliring.ForeColor = System.Drawing.Color.Lime;
            this.txtSandiWilayahKliring.Location = new System.Drawing.Point(819, 109);
            this.txtSandiWilayahKliring.Name = "txtSandiWilayahKliring";
            this.txtSandiWilayahKliring.Size = new System.Drawing.Size(249, 20);
            this.txtSandiWilayahKliring.TabIndex = 44;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(816, 95);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(154, 14);
            this.label12.TabIndex = 45;
            this.label12.Text = "Sandi Wilayah Kliring";
            // 
            // txtBatchId
            // 
            this.txtBatchId.BackColor = System.Drawing.Color.Black;
            this.txtBatchId.ForeColor = System.Drawing.Color.Lime;
            this.txtBatchId.Location = new System.Drawing.Point(607, 109);
            this.txtBatchId.Name = "txtBatchId";
            this.txtBatchId.Size = new System.Drawing.Size(100, 20);
            this.txtBatchId.TabIndex = 46;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(604, 95);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(63, 14);
            this.label13.TabIndex = 47;
            this.label13.Text = "Batch Id";
            // 
            // frmWSDL_Loader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1083, 596);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtBatchId);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtSandiWilayahKliring);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtSandiZona);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtSenderBIC);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtSandiKota);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtPeriode);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtBenefBIC);
            this.Controls.Add(this.btnEnkripResponse);
            this.Controls.Add(this.rtfResponse);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cboURL);
            this.Controls.Add(this.btnDekripDKE);
            this.Controls.Add(this.btnDekripResponse);
            this.Controls.Add(this.btnEnkripDKE);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtRequest);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtAuthcode);
            this.Controls.Add(this.txtBIC);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtDKE);
            this.Controls.Add(this.btnLoadWsdl);
            this.Controls.Add(this.tvwService);
            this.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "frmWSDL_Loader";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "MikroSoap";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnLoadWsdl;
        private System.Windows.Forms.TreeView tvwService;
        private System.Windows.Forms.TextBox txtDKE;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.TextBox txtBIC;
        private System.Windows.Forms.TextBox txtAuthcode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtRequest;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnEnkripDKE;
        private System.Windows.Forms.Button btnDekripResponse;
        private System.Windows.Forms.Button btnDekripDKE;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ComboBox cboURL;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox rtfResponse;
        private System.Windows.Forms.Button btnEnkripResponse;
        private System.Windows.Forms.TextBox txtBenefBIC;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPeriode;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtSandiKota;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtSenderBIC;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtSandiZona;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtSandiWilayahKliring;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtBatchId;
        private System.Windows.Forms.Label label13;
    }
}