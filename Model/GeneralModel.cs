﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TCPClientServer.Model
{
    public class GeneralModel
    {
        //private static frmServer formserver;
        //private static frmEnkripDekrip formEnkDek;
        public int TCPPort { get; set; }
        //private static Socket clientSocket ;
        //private static EnkDek enkdek = new EnkDek();
        public int MaxMsgLength { get; set; }
        public string WsdlUrl { get; set; }

        public int ThreadSleep { get; set; }

        //Path Settings
        public string Dummy_Path_Incoming { get; set; }
        public string Dummy_Path_Outgoing { get; set; }
        public string Dummy_Path_Parameter { get; set; }
        public string Log_Path { get; set; }

        public int KeepLogInDays { get; set; }
        public int StagnantHandler { get; set; }

        //settings
        public bool IsDummy_Incoming { get; set; }
        public bool IsDummy_Outgoing { get; set; }
        public bool IsDummy_Parameter { get; set; }

        public Boolean IsWriteLogToFile { get; set; }
        public bool IsEncBISPK { get; set; }
        public bool IsEncAS400 { get; set; }

        //Length Setting
        //public int RecordSize_getDaftarPesertaWilayah { get; set; }
        public int RecordCount_getDaftarPesertaWilayah { get; set; }

        //public int RecordSize_getDKEKreditInwardIndividual { get; set; }
        public int RecordCount_getDKEKreditInwardIndividual { get; set; }

        //messaging
        public string msg_length { get; set; }
        public string msg_type { get; set; }
        public string trans_code { get; set; }
        public string msg_seq { get; set; }
        public string bic { get; set; }
        public string authcode { get; set; }
        public string dke { get; set; }
        public string benefid { get; set; }
        public string periode { get; set; }
        public string sandikota { get; set; }

        //to TCP Socket
        public string msg_to_tcp { get; set; }
        public string msg_length_to_tcp { get; set; }
        public string msg_type_to_tcp { get; set; }
        public string transcode_to_tcp { get; set; }
        public string err_code_to_tcp { get; set; }
        public string err_desc_to_tcp { get; set; }
        public string status_toas { get; set; }
        public string msg_seq_to_tcp { get; set; }
        public string batch_id_to_tcp { get; set; }

        //to reply
        public byte[] bDKE { get; set; }
        public byte[] bispk_result { get; set; }
        public string sResultTo_TCPSocket { get; set; }
        public string sResult { get; set; }
        public string sAuthCodeBI { get; set; }
        public string sDkeBI { get; set; }
        public string sBenefId { get; set; }
        public string sPeriode { get; set; }
        public string sSandiKota { get; set; }
        public string sBatchId { get; set; }

        public string ReqSequence { get; set; }

        //component
        //public static CheckBox CheckBoxEncBISPK { get; set; }
        //public static CheckBox CheckBoxEncAS400 { get; set; }
        //public static CheckBox CheckBoxDummyIncoming { get; set; }
        //public static CheckBox CheckBoxDummyOutgoing { get; set; }
        //public static CheckBox CheckBoxDummyParameter { get; set; }
        //public static CheckBox CheckBoxWriteToFile { get; set; }
    }
}
