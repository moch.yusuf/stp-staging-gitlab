﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace TCPClientServer
{
    public class EnkDek
    {
        public string EnkripAS400(string Data)
        {
            try
            {

                Encoding MyEncoding = Encoding.GetEncoding("ASCII");

                AppSettingsReader settingsReader = new AppSettingsReader();
                string sKey = (string)settingsReader.GetValue("SecurityKeyToAS400", typeof(String));

                //Remove carriage returns
                Data = Data.Replace("\n", "").Replace("\r", "");

                //int mod4 = Data.Length % 4;
                //if (mod4 > 0)
                //{
                //    Data += new string('=', 4 - mod4);
                //}

                byte[] key;

                //MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                //key = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(sKey));
                ////Always release the resources and flush data
                //// of the Cryptographic service provide. Best Practice

                //hashmd5.Clear();

                key = MyEncoding.GetBytes(sKey);
                //byte[] iv = Encoding.ASCII.GetBytes("415A5A23B7C6962C");
                //byte[] data = StringToByteArray(Data);
                byte[] data = MyEncoding.GetBytes(Data);
                byte[] enc = new byte[0];

                //TripleDES tdes = TripleDES.Create();
                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();

                //tdes.IV = iv;
                tdes.Key = key;
                tdes.Mode = CipherMode.ECB;
                tdes.Padding = PaddingMode.PKCS7;

                ICryptoTransform ict = tdes.CreateEncryptor();
                enc = ict.TransformFinalBlock(data, 0, data.Length);

                return Convert.ToBase64String(enc);
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }

        public string DekripAS400(string Data)
        {
            try
            {
                Encoding MyEncoding = Encoding.GetEncoding("ASCII");

                AppSettingsReader settingsReader = new AppSettingsReader();
                string sKey = (string)settingsReader.GetValue("SecurityKeyToAS400", typeof(String));
                                
                Data = Data.Replace(" ", "+");
                int mod4 = Data.Length % 4;
                if (mod4 > 0)
                {
                    Data += new string('=', 4 - mod4);
                }

                byte[] bText = Convert.FromBase64String(Data);
                //byte[] bText = MyEncoding.GetBytes(Data);
                byte[] bKey = MyEncoding.GetBytes(sKey);
                //byte[] bVector = MyEncoding.GetBytes(intialVector);

                TripleDESCryptoServiceProvider tdesCSP = new TripleDESCryptoServiceProvider();

                tdesCSP.Key = bKey;
                //tdesCSP.IV = bVector;
                //tdesCSP.BlockSize = 64;
                //tdesCSP.KeySize = 192;
                //tdesCSP.FeedbackSize = 8;
                tdesCSP.Mode = CipherMode.ECB;
                tdesCSP.Padding = PaddingMode.PKCS7;

                ICryptoTransform crTfm = tdesCSP.CreateDecryptor();

                
                byte[] decrypted = crTfm.TransformFinalBlock(bText, 0, bText.Length);

                
                //var sdata2 = MyEncoding.GetString(decrypted, 0, decrypted.Length);
                //var sData1 = sdata2.Replace("|", Environment.NewLine);

                return MyEncoding.GetString(decrypted, 0, decrypted.Length);
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }

        }

        public string EnkripBISPK(string Data)
        {
            try
            {
                Encoding MyEncoding = Encoding.GetEncoding("ASCII");

                AppSettingsReader settingsReader = new AppSettingsReader();
                string sKey = (string)settingsReader.GetValue("SecurityKeyToBISPK", typeof(String));

                //Keep length
                int mod4 = Data.Length % 8;
                if (mod4 > 0)
                {
                    Data += new string(' ', 8 - mod4);
                }

                byte[] key = MyEncoding.GetBytes(sKey);
                byte[] data = MyEncoding.GetBytes(Data);
                byte[] enc = new byte[0];

                //TripleDES tdes = TripleDES.Create();
                TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();


                tdes.IV = new byte[8];
                tdes.Key = key;
                tdes.Mode = CipherMode.CBC;
                tdes.Padding = PaddingMode.None;

                ICryptoTransform ict = tdes.CreateEncryptor();
                enc = ict.TransformFinalBlock(data, 0, data.Length);

                return Convert.ToBase64String(enc);
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }

        public string DekripBISPK(string Data)
        {
            try
            {
                Encoding MyEncoding = Encoding.GetEncoding("ASCII");

                AppSettingsReader settingsReader = new AppSettingsReader();
                string sKey = (string)settingsReader.GetValue("SecurityKeyToBISPK", typeof(String));

                Data = Data.Replace(" ", "+");
                //int mod4 = Data.Length % 4;
                //if (mod4 > 0)
                //{
                //    Data += new string('=', 4 - mod4);
                //}

                byte[] bText = Convert.FromBase64String(Data);
                string sData = Convert.ToBase64String(bText);
                //byte[] bText = MyEncoding.GetBytes(Data);
                byte[] bKey = MyEncoding.GetBytes(sKey);
                //byte[] bVector = MyEncoding.GetBytes("556E697465644F7665727365617342616E6B496E646F6E65");

                TripleDESCryptoServiceProvider tdesCSP = new TripleDESCryptoServiceProvider();

                tdesCSP.Key = bKey;
                tdesCSP.IV = new byte[8];
                //tdesCSP.BlockSize = 64;
                //tdesCSP.KeySize = 192;
                //tdesCSP.FeedbackSize = 8;
                tdesCSP.Mode = CipherMode.CBC;
                tdesCSP.Padding = PaddingMode.None;

                ICryptoTransform crTfm = tdesCSP.CreateDecryptor();

                byte[] decrypted = crTfm.TransformFinalBlock(bText, 0, bText.Length);

                return MyEncoding.GetString(decrypted, 0, decrypted.Length);
                //return MyEncoding.GetString(bText);
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }

        public string Base64ToString(string Data)
        {
            try
            {
                byte[] bText = Convert.FromBase64String(Data);
                return Encoding.ASCII.GetString(bText);
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }

        public string StringToBase64(string Data)
        {
            try
            {
                byte[] bText = Encoding.ASCII.GetBytes(Data);
                return Convert.ToBase64String(bText);
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }

        //public string AddCRLF(string sDke)
        //{
        //    string sCRLF = null;
        //    string sDkeCurrent = null;
        //    int iCount = sDke.Length / 875;

        //    if(iCount < 0)
        //    {
        //        iCount = 1;
        //    }else
        //    {
        //        iCount = 2;
        //    }

        //    sDkeCurrent = sDke;

        //    for (int i=1; i<=iCount; i++)
        //    {
        //        sCRLF += sDkeCurrent.Left(875) + System.Environment.NewLine;
        //        sDkeCurrent = sDke.Replace(sCRLF.Replace("\n","").Replace("\r",""), "");
        //    }
                                   
        //    return sCRLF;
        //}

        public string ByteToString(byte[] Data)
        {
            try
            {
                return Encoding.ASCII.GetString(Data);
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }

        public string StringToBase64(byte[] Data)
        {
            try
            {
                return Convert.ToBase64String(Data);
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }

        public static string ConvertHex(String hexString)
        {
            try
            {
                string ascii = string.Empty;

                for (int i = 0; i < hexString.Length; i += 2)
                {
                    String hs = string.Empty;

                    hs = hexString.Substring(i, 2);
                    uint decval = System.Convert.ToUInt32(hs, 16);
                    char character = System.Convert.ToChar(decval);
                    ascii += character;

                }

                return ascii;
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }

            return string.Empty;
        }
    }
}
