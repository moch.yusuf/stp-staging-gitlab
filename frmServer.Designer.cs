﻿namespace TCPClientServer
{
    partial class frmServer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmServer));
            this.rtfLog = new System.Windows.Forms.RichTextBox();
            this.chkEncAS400 = new System.Windows.Forms.CheckBox();
            this.btnDevTools = new System.Windows.Forms.Button();
            this.chkEncBISPK = new System.Windows.Forms.CheckBox();
            this.btnSoapUI = new System.Windows.Forms.Button();
            this.chkDummyIncoming = new System.Windows.Forms.CheckBox();
            this.chkLogFile = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkDummyParameter = new System.Windows.Forms.CheckBox();
            this.chkDummyOutgoing = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // rtfLog
            // 
            this.rtfLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtfLog.BackColor = System.Drawing.Color.Black;
            this.rtfLog.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtfLog.ForeColor = System.Drawing.Color.Lime;
            this.rtfLog.Location = new System.Drawing.Point(14, 123);
            this.rtfLog.Name = "rtfLog";
            this.rtfLog.Size = new System.Drawing.Size(844, 416);
            this.rtfLog.TabIndex = 0;
            this.rtfLog.Text = "";
            // 
            // chkEncAS400
            // 
            this.chkEncAS400.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkEncAS400.AutoSize = true;
            this.chkEncAS400.Checked = true;
            this.chkEncAS400.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkEncAS400.ForeColor = System.Drawing.Color.White;
            this.chkEncAS400.Location = new System.Drawing.Point(16, 19);
            this.chkEncAS400.Name = "chkEncAS400";
            this.chkEncAS400.Size = new System.Drawing.Size(124, 18);
            this.chkEncAS400.TabIndex = 1;
            this.chkEncAS400.Text = "Enkripsi AS400";
            this.chkEncAS400.UseVisualStyleBackColor = true;
            // 
            // btnDevTools
            // 
            this.btnDevTools.BackColor = System.Drawing.Color.Black;
            this.btnDevTools.ForeColor = System.Drawing.Color.White;
            this.btnDevTools.Location = new System.Drawing.Point(14, 9);
            this.btnDevTools.Name = "btnDevTools";
            this.btnDevTools.Size = new System.Drawing.Size(87, 25);
            this.btnDevTools.TabIndex = 2;
            this.btnDevTools.Text = "Dev Tool\'s";
            this.btnDevTools.UseVisualStyleBackColor = false;
            this.btnDevTools.Click += new System.EventHandler(this.btnDevTools_Click);
            // 
            // chkEncBISPK
            // 
            this.chkEncBISPK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkEncBISPK.AutoSize = true;
            this.chkEncBISPK.ForeColor = System.Drawing.Color.White;
            this.chkEncBISPK.Location = new System.Drawing.Point(16, 43);
            this.chkEncBISPK.Name = "chkEncBISPK";
            this.chkEncBISPK.Size = new System.Drawing.Size(131, 18);
            this.chkEncBISPK.TabIndex = 3;
            this.chkEncBISPK.Text = "Enkripsi BI SPK";
            this.chkEncBISPK.UseVisualStyleBackColor = true;
            this.chkEncBISPK.CheckedChanged += new System.EventHandler(this.chkEncBISPK_CheckedChanged);
            // 
            // btnSoapUI
            // 
            this.btnSoapUI.BackColor = System.Drawing.Color.Black;
            this.btnSoapUI.ForeColor = System.Drawing.Color.White;
            this.btnSoapUI.Location = new System.Drawing.Point(108, 9);
            this.btnSoapUI.Name = "btnSoapUI";
            this.btnSoapUI.Size = new System.Drawing.Size(87, 25);
            this.btnSoapUI.TabIndex = 4;
            this.btnSoapUI.Text = "MikroSoap";
            this.btnSoapUI.UseVisualStyleBackColor = false;
            this.btnSoapUI.Click += new System.EventHandler(this.btnSoapUI_Click);
            // 
            // chkDummyIncoming
            // 
            this.chkDummyIncoming.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkDummyIncoming.AutoSize = true;
            this.chkDummyIncoming.ForeColor = System.Drawing.Color.White;
            this.chkDummyIncoming.Location = new System.Drawing.Point(16, 19);
            this.chkDummyIncoming.Name = "chkDummyIncoming";
            this.chkDummyIncoming.Size = new System.Drawing.Size(82, 18);
            this.chkDummyIncoming.TabIndex = 5;
            this.chkDummyIncoming.Text = "Incoming";
            this.chkDummyIncoming.UseVisualStyleBackColor = true;
            // 
            // chkLogFile
            // 
            this.chkLogFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkLogFile.AutoSize = true;
            this.chkLogFile.Checked = true;
            this.chkLogFile.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLogFile.ForeColor = System.Drawing.Color.White;
            this.chkLogFile.Location = new System.Drawing.Point(16, 67);
            this.chkLogFile.Name = "chkLogFile";
            this.chkLogFile.Size = new System.Drawing.Size(145, 18);
            this.chkLogFile.TabIndex = 6;
            this.chkLogFile.Text = "Write log to file";
            this.chkLogFile.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkDummyParameter);
            this.groupBox1.Controls.Add(this.chkDummyOutgoing);
            this.groupBox1.Controls.Add(this.chkDummyIncoming);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(644, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(214, 100);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dummy Reply";
            // 
            // chkDummyParameter
            // 
            this.chkDummyParameter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkDummyParameter.AutoSize = true;
            this.chkDummyParameter.ForeColor = System.Drawing.Color.White;
            this.chkDummyParameter.Location = new System.Drawing.Point(16, 67);
            this.chkDummyParameter.Name = "chkDummyParameter";
            this.chkDummyParameter.Size = new System.Drawing.Size(89, 18);
            this.chkDummyParameter.TabIndex = 7;
            this.chkDummyParameter.Text = "Parameter";
            this.chkDummyParameter.UseVisualStyleBackColor = true;
            // 
            // chkDummyOutgoing
            // 
            this.chkDummyOutgoing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkDummyOutgoing.AutoSize = true;
            this.chkDummyOutgoing.ForeColor = System.Drawing.Color.White;
            this.chkDummyOutgoing.Location = new System.Drawing.Point(16, 43);
            this.chkDummyOutgoing.Name = "chkDummyOutgoing";
            this.chkDummyOutgoing.Size = new System.Drawing.Size(82, 18);
            this.chkDummyOutgoing.TabIndex = 6;
            this.chkDummyOutgoing.Text = "Outgoing";
            this.chkDummyOutgoing.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkEncAS400);
            this.groupBox2.Controls.Add(this.chkEncBISPK);
            this.groupBox2.Controls.Add(this.chkLogFile);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(428, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(210, 100);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Settings";
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "STP Staging";
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // frmServer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(873, 553);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnSoapUI);
            this.Controls.Add(this.btnDevTools);
            this.Controls.Add(this.rtfLog);
            this.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmServer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TCP Monitor v2.0";
            this.Load += new System.EventHandler(this.frmServer_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.RichTextBox rtfLog;
        private System.Windows.Forms.Button btnDevTools;
        public System.Windows.Forms.CheckBox chkEncAS400;
        public System.Windows.Forms.CheckBox chkEncBISPK;
        private System.Windows.Forms.Button btnSoapUI;
        public System.Windows.Forms.CheckBox chkDummyIncoming;
        public System.Windows.Forms.CheckBox chkLogFile;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.CheckBox chkDummyParameter;
        public System.Windows.Forms.CheckBox chkDummyOutgoing;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.NotifyIcon notifyIcon1;
    }
}

