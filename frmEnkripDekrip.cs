﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Permissions;
using System.Web.Services.Description;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.CodeDom.Compiler;
using System.Reflection;
using System.Configuration;
using TCPClientServer.General;
using System.IO;

namespace TCPClientServer
{
    public partial class frmEnkripDekrip : Form
    {
        public EnkDek enkdek = new EnkDek();
        public frmEnkripDekrip()
        {
            InitializeComponent();
        }

        private void btnEnkrip_Click(object sender, EventArgs e)
        {
            if (chkEncryptedYN.Checked)
            {
                if (rdAS400_Staging.Checked)
                {
                    rtfOutput.Text = enkdek.EnkripAS400(rtfInput.Text);
                }
                else
                {
                    rtfOutput.Text = enkdek.EnkripBISPK(rtfInput.Text);
                }
            }
            else
            {
                rtfOutput.Text = enkdek.StringToBase64(rtfInput.Text);
            }
        }

        private void btnDekrip_Click(object sender, EventArgs e)
        {
            if (chkEncryptedYN.Checked)
            {
                if (rdAS400_Staging.Checked)
                {
                    rtfOutput.Text = enkdek.DekripAS400(rtfInput.Text);
                }
                else
                {
                    rtfOutput.Text = enkdek.DekripBISPK(rtfInput.Text);
                }
            }
            else
            {
                rtfOutput.Text = enkdek.Base64ToString(rtfInput.Text);
            }
        }

        private void frmEnkripDekrip_Load(object sender, EventArgs e)
        {

        }

        private void frmEnkripDekrip_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized)
            {

            }
            else if (WindowState == FormWindowState.Maximized)
            {
                groupBox1.Left = (this.ClientSize.Width - groupBox1.Width) / 2;
                groupBox1.Top = (this.ClientSize.Height - groupBox1.Height) / 2;
            }
        }

        protected override void OnResize(EventArgs e)
        {
            groupBox1.Left = (this.ClientSize.Width - groupBox1.Width) / 2;
            //groupBox1.Top = (this.ClientSize.Height - groupBox1.Height) / 2;

            btnDekrip.Left = (this.ClientSize.Width - btnDekrip.Width) / 2;
            btnEnkrip.Left = (this.ClientSize.Width - btnEnkrip.Width) / 2;

            chkEncryptedYN.Left = (this.ClientSize.Width - chkEncryptedYN.Width) / 2;

            rtfInput.Height = (this.ClientSize.Height) - 60;
            rtfInput.Width = (this.ClientSize.Width / 2) - groupBox1.Width;
            //rtfOutput.Left = (this.ClientSize.Width / 2) + groupBox1.Width;

            rtfOutput.Height = (this.ClientSize.Height) - 60;
            rtfOutput.Width = (this.ClientSize.Width / 2) - groupBox1.Width;

            rtfOutput.Left = (this.ClientSize.Width / 2) + (groupBox1.Width - 15);
            //rtfOutput.Left = rtfInput.Left;

            label1.Left = (rtfInput.Width / 2) - 15;
            label2.Left = (this.ClientSize.Width) - (rtfOutput.Width / 2) - 40;
        }

        private void btnAddCRLF_Click(object sender, EventArgs e)
        {
            //rtfOutput.Text = enkdek.AddCRLF(rtfInput.Text);
        }

        private void potong()
        {
            List<string> listResult = new List<string>();
            List<string> listTemp = new List<string>();

            String[] listDefault;
            String sTemp = "";

            var vInput = rtfInput.Text.Replace("\u001a", "");

            Char[] separator = { '\n' };
            listDefault = vInput.Split(separator);

            var temp = "";
            foreach (String list in listDefault)
            {
                if (!String.IsNullOrEmpty(list))
                {                    
                    switch (list.Substring(0, 2))
                    {
                        //header
                        case "01":
                            temp = list.Insert(83, setSpace(792));
                            break;
                        
                        //footer
                        case "31":
                            temp = list.Insert(7, setSpace(868));
                            break;
                        
                         //detail
                        default:
                            temp = list;
                            break;
                    }

                    listTemp.Add(temp);
                }
            }


            double iTempLength = listTemp.ToArray().Count();
            //double iRecCount = Math.Ceiling(iTempLength / 50);
            double iRecCount = Math.Ceiling(iTempLength / 3000);

            int x = 0;
            for (int i = 0; i < iRecCount; i++)
            {
                sTemp = "";
                for (int j = x; j < iTempLength; j++)
                {
                    sTemp += listTemp.ElementAtOrDefault(j) + "\n";
                    if (j >= (x + 2999))
                    {
                        x = j+1;
                        break;
                    }
                }

                listResult.Add(sTemp);
            }

            ////var vInput = rtfInput.Text.Replace("\n", "");
            ////var vInput = rtfInput.Text.Replace("\u001a", "");
            //var sSpace = "                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ";
            ////vInput = rtfInput.Text.Insert(83, sSpace);
            //int iCount = (vInput.Length / 875) / 50;

            //int iStartIndex = 0;

            ////int iLength = 877 * 50;

            ////((875 + 2(CRLF)) * 50) - 2(CRLF paling bawah)
            //int iLength = 877 * 50;

            ////hitung sisa record yg < 50
            ////iCount += ((vInput.Length / 875) % 50) > 0 ? 1 : 0;
            //iCount += ((vInput.Length / 877) % 50) > 0 ? 1 : 0;
            ////string vInput2 = vInput + new string('#', ((vInput.Length / 12) % 50));


            //int mod4 = vInput.Length % iLength;
            //if (mod4 > 0)
            //{
            //    vInput += new string('#', iLength - mod4);
            //}


            //for (int i = 1; i <= iCount; i++)
            //{
            //    listResult.Add(vInput.Substring(iStartIndex, iLength));

            //    iStartIndex = iStartIndex + iLength;
            //    //iLength = iLength * 2;
            //}

            rtfOutput.Text = "";

            foreach (var list in listResult)
            {
                rtfOutput.AppendText(list);
            }
        }

        private String setSpace(int count)
        {
            var vSpace = "";

            for (int i = 1; i <= count; i++)
            {
                vSpace += " ";
            }

            return vSpace;
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            MessageProcessing mp = new MessageProcessing();
            //rtfInput.Text = mp.GetDKEKreditInwardIndividual_RecSize.RecordSize_getDKEKreditInwardIndividual.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            potong();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //potong();
            Program.clearLog(7);
        }

        private void button1_Click_3(object sender, EventArgs e)
        {
            potong();
        }

        private void button1_Click_4(object sender, EventArgs e)
        {
            potong();
        }

        private void button1_Click_5(object sender, EventArgs e)
        {
            
        }

        private void button1_Click_6(object sender, EventArgs e)
        {
            Program.RestartService();
        }

        private void button1_Click_7(object sender, EventArgs e)
        {

            //Program.RestartService();
            potong();
        }

        private void button1_Click_8(object sender, EventArgs e)
        {
            Program.clearLog(7);
        }

        private void button1_Click_9(object sender, EventArgs e)
        {
            string str = "0000534040010000BBIJIDJATO9dHJsWlb0F2t0VaZM8WQ==000001";
            rtfOutput.Text = str.Substring(0, 5);
        }



        //private void btnLoadWsdl_Click(object sender, EventArgs e)
        //{
        //    AppSettingsReader settingsReader = new AppSettingsReader();
        //    string WSDL_Url = (string)settingsReader.GetValue("WSDL_Url", typeof(string));

        //    LoadWebService(WSDL_Url);
        //}

        //[SecurityPermissionAttribute(SecurityAction.Demand, Unrestricted = true)]
        //internal static void LoadWebService(string webServiceAsmxUrl)
        //{
        //    ParseUrl(webServiceAsmxUrl);
        //    System.Net.WebClient client = new System.Net.WebClient();
        //    // Connect To the web service
        //    System.IO.Stream stream = client.OpenRead(webServiceAsmxUrl + "?wsdl");
        //    // Now read the WSDL file describing a service.
        //    ServiceDescription description = ServiceDescription.Read(stream);
        //    ///// LOAD THE DOM /////////
        //    // Initialize a service description importer.
        //    ServiceDescriptionImporter importer = new ServiceDescriptionImporter();
        //    importer.ProtocolName = "Soap12"; // Use SOAP 1.2.
        //    importer.AddServiceDescription(description, null, null);
        //    // Generate a proxy client.
        //    importer.Style = ServiceDescriptionImportStyle.Client;
        //    // Generate properties to represent primitive values.
        //    importer.CodeGenerationOptions = System.Xml.Serialization.CodeGenerationOptions.GenerateProperties;
        //    // Initialize a Code-DOM tree into which we will import the service.
        //    CodeNamespace nmspace = new CodeNamespace();
        //    CodeCompileUnit unit1 = new CodeCompileUnit();
        //    unit1.Namespaces.Add(nmspace);
        //    // Import the service into the Code-DOM tree. This creates proxy code that uses the service.
        //    ServiceDescriptionImportWarnings warning = importer.Import(nmspace, unit1);
        //    if (warning == 0) // If zero then we are good to go
        //    {
        //        // Generate the proxy code
        //        CodeDomProvider provider1 = CodeDomProvider.CreateProvider("CSharp");
        //        // Compile the assembly proxy with the appropriate references
        //        string[] assemblyReferences = new string[5] { "System.dll", "System.Web.Services.dll", "System.Web.dll", "System.Xml.dll", "System.Data.dll" };
        //        CompilerParameters parms = new CompilerParameters(assemblyReferences);
        //        CompilerResults results = provider1.CompileAssemblyFromDom(parms, unit1);
        //        // Check For Errors
        //        if (results.Errors.Count > 0)
        //        {
        //            foreach (CompilerError oops in results.Errors)
        //            {
        //                System.Diagnostics.Debug.WriteLine("========Compiler error============");
        //                System.Diagnostics.Debug.WriteLine(oops.ErrorText);
        //            }
        //            Console.WriteLine("Compile Error Occured calling webservice. Check Debug ouput window.");
        //        }
        //        // Finally, add the web service method to our list of methods to test
        //        //--------------------------------------------------------------------------------------------
        //        object service = results.CompiledAssembly.CreateInstance(serviceName);
        //        Type types = service.GetType();
        //        List<MethodInfo> listMethods = types.GetMethods().ToList();
        //    }
        //}

    }
}
