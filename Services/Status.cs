﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Services.Protocols;
using TCPClientServer.General;
using TCPClientServer.Model;

namespace TCPClientServer.Services
{
    public class Status
    {
        private static EnkDek enkdek = new EnkDek();
        private static MessageProcessing mp = new MessageProcessing();

        public Status() { }

        public string getStatus(GeneralModel gm)
        {
            Program.writeLog("Message Length    : " + gm.msg_length);
            Program.writeLog("Message Type      : " + gm.msg_type);
            Program.writeLog("Transaction Code  : " + gm.trans_code);
            Program.writeLog("Message Sequence  : " + gm.msg_seq);
            Program.writeLog("BIC               : " + gm.bic);
            Program.writeLog("Authorization     : " + gm.authcode);
            Program.writeLog("Batch Id          : " + gm.sBatchId);

            gm.msg_type_to_tcp = "404";

            switch (gm.trans_code)
            {
                case "001":
                    try
                    {
                        gm.bispk_result = Program._wsdl.getStatusDKEKreditOutwardIndividual(gm.bic, gm.sAuthCodeBI, gm.sBatchId);
                    }
                    catch (Exception soe)
                    {
                        Program.writeLog("ALERT002: Koneksi dari Staging ke BI SPK tidak establish");
                        return null;
                    }
                    break;
            }

            gm.sResult = enkdek.StringToBase64(gm.bispk_result);

            if (gm.IsEncBISPK)
            {
                gm.sResult = enkdek.DekripBISPK(gm.sResult);
            }
            else
            {
                gm.sResult = enkdek.Base64ToString(gm.sResult);
            }

            if (gm.sResult.Trim() != "")
                gm.sResultTo_TCPSocket = enkdek.EnkripAS400(gm.sResult);
            else
                gm.sResultTo_TCPSocket = "";

            if (!gm.IsEncAS400)
            {
                gm.sResultTo_TCPSocket = enkdek.DekripAS400(gm.sResult);
            }

            gm.sResultTo_TCPSocket = enkdek.EnkripAS400(gm.sResult);

            //BI Parameter
            Program.writeLog("Response from BI        : " + gm.sResult);
            //end BI Parameter

            //if no error (error code is 4 digit)
            if (gm.sResult.Trim().Length > 4)
            {
                gm.msg_seq_to_tcp = "0000";
                gm.transcode_to_tcp = gm.trans_code;

                gm.msg_seq_to_tcp = "0000";
                gm.transcode_to_tcp = gm.trans_code;
                
                gm.err_code_to_tcp = "      ";
                gm.err_desc_to_tcp = "                                                  ";

                gm.msg_to_tcp = gm.msg_type_to_tcp + gm.transcode_to_tcp + gm.msg_seq_to_tcp + gm.batch_id_to_tcp + gm.err_code_to_tcp + gm.err_desc_to_tcp + gm.sResultTo_TCPSocket;

                gm.msg_length_to_tcp = ("000000" + (gm.msg_to_tcp.Length + 6)).Right(6);

                gm.msg_to_tcp = gm.msg_length_to_tcp + gm.msg_to_tcp;
            }
            else
            {
                gm.msg_seq_to_tcp = "0000";
                gm.transcode_to_tcp = gm.trans_code;

                gm.msg_seq_to_tcp = "0000";
                gm.transcode_to_tcp = gm.trans_code;

                gm.err_code_to_tcp = enkdek.DekripAS400(gm.sResultTo_TCPSocket);
                gm.err_desc_to_tcp = "                                                  ";

                gm.msg_to_tcp = gm.msg_type_to_tcp + gm.transcode_to_tcp + gm.msg_seq_to_tcp + gm.batch_id_to_tcp + gm.err_code_to_tcp;

                gm.msg_length_to_tcp = ("000000" + (gm.msg_to_tcp.Length + 6)).Right(6);

                gm.msg_to_tcp = gm.msg_length_to_tcp + gm.msg_to_tcp;
            }

            return gm.msg_to_tcp;
        }
    }
}
