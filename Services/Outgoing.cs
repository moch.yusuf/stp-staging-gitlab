﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Services.Protocols;
using TCPClientServer.Model;

namespace TCPClientServer.Services
{
    public class Outgoing
    {
        private static EnkDek enkdek = new EnkDek();
        
        public Outgoing()
        {
        }

        public string SendDKEKreditIndividual(GeneralModel gm)
        {
            Program.writeLog("Message Length    : " + gm.msg_length);
            Program.writeLog("Message Type      : " + gm.msg_type);
            Program.writeLog("Transaction Code  : " + gm.trans_code);
            Program.writeLog("Message Sequence  : " + gm.msg_seq);
            Program.writeLog("BIC               : " + gm.bic);
            Program.writeLog("Authorization     : " + gm.authcode);
            Program.writeLog("Batch No          : " + gm.sBatchId);
            Program.writeLog("DKE               : " + gm.dke);

            gm.msg_type_to_tcp = "101";

            switch (gm.trans_code)
            {
                case "001":
                    if (gm.IsDummy_Outgoing)
                    {
                        var filenames = Directory.GetFiles(gm.Dummy_Path_Outgoing, "*.*");
                        byte[] file = null;

                        for (int i = 0; i < filenames.Length; i++)
                        {
                            file = File.ReadAllBytes(filenames[i]);
                        }

                        gm.bispk_result = file;
                    }
                    else
                    {
                        try
                        {
                            gm.bispk_result = Program._wsdl.sendDKEKreditIndividual(gm.bic, gm.sAuthCodeBI, gm.bDKE);
                        }
                        catch (Exception soe)
                        {
                            Program.writeLog("ALERT002: Koneksi dari Staging ke BI SPK tidak establish");
                            return null;
                        }
                    }

                    break;
                    
                    //case "002":
                    //    result = _wsdl.sendDKEPenyerahanIndividual(bic, sAuthCodeBI, bDKE);
                    //    break;
                    //case "003":
                    //    result = _wsdl.sendDKEPengembalianIndividual(bic, sAuthCodeBI, bDKE);
                    //    break;
            }

            gm.sResult = enkdek.StringToBase64(gm.bispk_result);

            Program.writeLog("Response from BI ORI          : " + gm.sResult);

            if (gm.IsEncBISPK)
            {
                gm.sResult = enkdek.DekripBISPK(gm.sResult);
            }
            else
            {
                gm.sResult = enkdek.Base64ToString(gm.sResult);
            }

            gm.sResultTo_TCPSocket = enkdek.EnkripAS400(gm.sResult);

            if (!gm.IsEncAS400)
            {
                gm.sResultTo_TCPSocket = enkdek.DekripAS400(gm.sResult);
            }

            //BI Parameter
            //writeLog("AuthCode to BI          : " + sAuthCodeBI);
            Program.writeLog("DKE to BI               : " + gm.sDkeBI);
            Program.writeLog("Response from BI        : " + gm.sResult);
            //end BI Parameter

            //if no error (error code is 4 digit)
            if (gm.sResult.Trim().Length > 4)
            {
                gm.msg_seq_to_tcp = "0000";
                gm.transcode_to_tcp = gm.trans_code;

                //Program.writeLog("Response                : " + gm.sResultTo_TCPSocket);

                gm.msg_seq_to_tcp = "0000";
                gm.transcode_to_tcp = gm.trans_code;


                gm.err_code_to_tcp = "      ";
                gm.err_desc_to_tcp = "                                                  ";

                gm.msg_to_tcp = gm.msg_type_to_tcp + gm.transcode_to_tcp + gm.msg_seq_to_tcp + gm.err_code_to_tcp + gm.err_desc_to_tcp + gm.batch_id_to_tcp + gm.sResultTo_TCPSocket;

                gm.msg_length_to_tcp = ("000000" + (gm.msg_to_tcp.Length + 6)).Right(6);

                gm.msg_to_tcp = gm.msg_length_to_tcp + gm.msg_to_tcp;
            }
            else
            {
                gm.msg_seq_to_tcp = "0000";
                gm.transcode_to_tcp = gm.trans_code;

                //Program.writeLog("Response                : " + gm.sResultTo_TCPSocket);

                gm.msg_seq_to_tcp = "0000";
                gm.transcode_to_tcp = gm.trans_code;

                gm.err_code_to_tcp = enkdek.DekripAS400(gm.sResultTo_TCPSocket);
                gm.err_desc_to_tcp = "                                                  ";

                gm.msg_to_tcp = gm.msg_type_to_tcp + gm.transcode_to_tcp + gm.msg_seq_to_tcp + gm.err_code_to_tcp + gm.err_desc_to_tcp + gm.batch_id_to_tcp;

                gm.msg_length_to_tcp = ("000000" + (gm.msg_to_tcp.Length + 6)).Right(6);

                gm.msg_to_tcp = gm.msg_length_to_tcp + gm.msg_to_tcp;
            }

            //reply
            return gm.msg_to_tcp;
        }
    }
}
