﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Services.Protocols;
using TCPClientServer.General;
using TCPClientServer.Model;

namespace TCPClientServer.Services
{
    public class Incoming
    {
        private static EnkDek enkdek = new EnkDek();

        private static MessageProcessing mp = new MessageProcessing();

        public Incoming() { }

        public string getDKEKreditInwardIndividual(GeneralModel gm)
        {
            Program.writeLog("Message Length    : " + gm.msg_length);
            Program.writeLog("Message Type      : " + gm.msg_type);
            Program.writeLog("Transaction Code  : " + gm.trans_code);
            Program.writeLog("Message Sequence  : " + gm.msg_seq);
            Program.writeLog("BIC               : " + gm.bic);
            Program.writeLog("Authorization     : " + gm.authcode);
            Program.writeLog("Benef Id          : " + gm.sBenefId);
            Program.writeLog("Periode           : " + gm.sPeriode);
            Program.writeLog("Sandi Kota        : " + gm.sSandiKota);
            Program.writeLog("Sequence          : " + gm.ReqSequence);

            gm.msg_type_to_tcp = "202";

            switch (gm.trans_code)
            {
                case "001":
                    if (gm.IsDummy_Incoming)
                    {
                        var filenames = Directory.GetFiles(gm.Dummy_Path_Incoming, "*.*");
                        byte[] file = null;

                        for (int i = 0; i < filenames.Length; i++)
                        {
                            file = File.ReadAllBytes(filenames[i]);
                        }

                        gm.bispk_result = file;
                    }
                    else
                    {
                        try
                        {
                            gm.bispk_result = Program._wsdl.getDKEKreditInwardIndividual(gm.bic, gm.sAuthCodeBI, gm.sBenefId, gm.sPeriode, gm.sSandiKota);
                        }
                        catch (Exception soe)
                        {
                            Program.writeLog("ALERT002: Koneksi dari Staging ke BI SPK tidak establish");
                            return null;
                        }
                    }

                    break;
            }

            gm.sResult = enkdek.StringToBase64(gm.bispk_result);

            Program.writeLog("Response from BI ORI          : " + gm.sResult);

            if (gm.IsEncBISPK)
            {
                gm.sResult = enkdek.DekripBISPK(gm.sResult);
            }
            else
            {
                gm.sResult = enkdek.Base64ToString(gm.sResult);
            }

            if (gm.sResult.Trim() != "")
                gm.sResultTo_TCPSocket = enkdek.EnkripAS400(gm.sResult);
            else
                gm.sResultTo_TCPSocket = "";

            if (!gm.IsEncAS400)
            {
                gm.sResultTo_TCPSocket = enkdek.DekripAS400(gm.sResult);
            }

            //BI Parameter
            Program.writeLog("Response from BI        : " + gm.sResult);
            //end BI Parameter

            //if no error (error code is 4 digit)
            if (gm.sResult.Trim().Length > 4)
            {
                gm.msg_seq_to_tcp = "0000";
                gm.transcode_to_tcp = gm.trans_code;

                //Program.writeLog("Response                : " + gm.sResultTo_TCPSocket);

                gm.msg_seq_to_tcp = "0000";
                gm.transcode_to_tcp = gm.trans_code;

                gm.err_code_to_tcp = "      ";
                gm.err_desc_to_tcp = "                                                  ";
                //begin DSE
                gm.msg_to_tcp = gm.msg_type_to_tcp + gm.transcode_to_tcp + gm.msg_seq_to_tcp + gm.bic + gm.sandikota + gm.benefid + gm.periode + gm.ReqSequence + gm.err_code_to_tcp + gm.err_desc_to_tcp + gm.sResultTo_TCPSocket;
                //end DSR
                gm.msg_length_to_tcp = ("000000" + (gm.msg_to_tcp.Length + 6)).Right(6);

                gm.msg_to_tcp = gm.msg_length_to_tcp + gm.msg_to_tcp;
            }
            //error code
            else
            {
                gm.msg_seq_to_tcp = "0000";
                gm.transcode_to_tcp = gm.trans_code;

                //Program.writeLog("Response                : " + gm.sResultTo_TCPSocket);

                gm.msg_seq_to_tcp = "0000";
                gm.transcode_to_tcp = gm.trans_code;

                //begin DSR
                if (gm.sResultTo_TCPSocket.Trim() != "")
                {
                    gm.err_code_to_tcp = enkdek.DekripAS400(gm.sResultTo_TCPSocket);
                    gm.err_desc_to_tcp = "                                                  ";
                }
                else
                {
                    gm.err_code_to_tcp = "ST01  ";
                    gm.err_desc_to_tcp = "Tidak ada data dari BI                            ";
                }

                gm.msg_to_tcp = gm.msg_type_to_tcp + gm.transcode_to_tcp + gm.msg_seq_to_tcp + gm.bic + gm.benefid + gm.sandikota + gm.periode + gm.ReqSequence + gm.err_code_to_tcp + gm.err_desc_to_tcp;
                //end DSR

                gm.msg_length_to_tcp = ("000000" + (gm.msg_to_tcp.Length + 6)).Right(6);

                gm.msg_to_tcp = gm.msg_length_to_tcp + gm.msg_to_tcp;
            }

            //if (gm.sResult.Trim().Length > mp.GetMaxMsgLength.MaxMsgLength)
            //{
            switch (gm.trans_code)
            {
                case "001":

                    //recall setting
                    //ConfigurationManager.RefreshSection("appSettings");
                    //Console.WriteLine(ConfigurationManager.AppSettings["myKey"]);

                    //MessageProcessing mp = new MessageProcessing();
                    //iThreadSleep = mp.GetThreadSleep.ThreadSleep;

                    //iRecSize_getDKEKreditInwardIndividual = mp.GetDKEKreditInwardIndividual_RecSize.RecordSize_getDKEKreditInwardIndividual;
                    //iRecCount_getDKEKreditInwardIndividual = mp.GetDKEKreditInwardIndividual_RecCount.RecordCount_getDKEKreditInwardIndividual;


                    List<string> listData = new List<string>();
                    //listData = mp.GetPartialData(gm.sResult, iRecLength_getDKEKreditInwardIndividual, iRecSize_getDKEKreditInwardIndividual);
                    listData = mp.GetPartialData(gm.sResult, gm.RecordCount_getDKEKreditInwardIndividual);

                    int index = 1;
                    foreach (var list in listData)
                    {
                        if (index == listData.ToArray().Length)
                        {
                            gm.msg_seq_to_tcp = "9999";
                        }
                        else
                        {
                            gm.msg_seq_to_tcp = ("0000" + index).Right(4);
                        }

                        //fix length        3        |         3        |       4        |     6           |     50          |      64400
                        //gm.msg_to_tcp = gm.msg_type_to_tcp + gm.transcode_to_tcp + gm.msg_seq_to_tcp + gm.err_code_to_tcp + gm.err_desc_to_tcp + enkdek.EnkripAS400(list.Replace("#", ""));
                        //gm.msg_to_tcp = gm.msg_type_to_tcp + gm.transcode_to_tcp + gm.msg_seq_to_tcp + gm.bic + gm.sandikota + gm.benefid + gm.periode + gm.ReqSequence + gm.err_code_to_tcp + gm.err_desc_to_tcp + enkdek.EnkripAS400(list.Replace("#", ""));
                        gm.msg_to_tcp = gm.msg_type_to_tcp + gm.transcode_to_tcp + gm.msg_seq_to_tcp + gm.bic + gm.sandikota + gm.benefid + gm.periode + gm.ReqSequence + gm.err_code_to_tcp + gm.err_desc_to_tcp + enkdek.EnkripAS400(list);

                        gm.msg_length_to_tcp = ("000000" + (gm.msg_to_tcp.Length + 6)).Right(6);

                        //fix length        6          
                        gm.msg_to_tcp = gm.msg_length_to_tcp + gm.msg_to_tcp;

                        //return gm.msg_to_tcp;
                        Program.sendReply(gm.msg_to_tcp);

                        index++;

                        Thread.Sleep(gm.ThreadSleep);
                    }

                    break;
            }
            //}
            //else
            //{
            //    Program.sendReply(gm.msg_to_tcp);
            //}

            return gm.msg_to_tcp;
        }
    }
}
