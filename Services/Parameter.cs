﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Services.Protocols;
using TCPClientServer.General;
using TCPClientServer.Model;

namespace TCPClientServer.Services
{
    public class Parameter
    {
        private static EnkDek enkdek = new EnkDek();
        
        private static MessageProcessing mp = new MessageProcessing();

        public Parameter() { }

        public string getParameter(GeneralModel gm)
        {
            Program.writeLog("Message Length    : " + gm.msg_length);
            Program.writeLog("Message Type      : " + gm.msg_type);
            Program.writeLog("Transaction Code  : " + gm.trans_code);
            Program.writeLog("BIC               : " + gm.bic);
            Program.writeLog("Authorization     : " + gm.authcode);

            gm.msg_type_to_tcp = "303";

            switch (gm.trans_code)
            {
                case "001":
                    if (gm.IsDummy_Parameter)
                    {
                        var filenames = Directory.GetFiles(gm.Dummy_Path_Parameter, "*.001");
                        byte[] file = null;

                        for (int i = 0; i < filenames.Length; i++)
                        {
                            file = File.ReadAllBytes(filenames[i]);
                        }

                        gm.bispk_result = file;
                    }
                    else
                    {
                        try
                        {
                            gm.bispk_result = Program._wsdl.getDaftarPeserta(gm.bic, gm.sAuthCodeBI);
                        }
                        catch (Exception soe)
                        {
                            Program.writeLog("ALERT002: Koneksi dari Staging ke BI SPK tidak establish");
                            return null;
                        }
                    }

                    break;

                case "002":
                    if (gm.IsDummy_Parameter)
                    {
                        var filenames = Directory.GetFiles(gm.Dummy_Path_Parameter, "*.002");
                        byte[] file = null;

                        for (int i = 0; i < filenames.Length; i++)
                        {
                            file = File.ReadAllBytes(filenames[i]);
                        }

                        gm.bispk_result = file;
                    }
                    else
                    {
                        gm.bispk_result = Program._wsdl.getDaftarPesertaWilayah(gm.bic, gm.sAuthCodeBI);
                    }

                    break;

                case "003":
                    if (gm.IsDummy_Parameter)
                    {
                        var filenames = Directory.GetFiles(gm.Dummy_Path_Parameter, "*.003");
                        byte[] file = null;

                        for (int i = 0; i < filenames.Length; i++)
                        {
                            file = File.ReadAllBytes(filenames[i]);
                        }

                        gm.bispk_result = file;
                    }
                    else
                    {
                        gm.bispk_result = Program._wsdl.getDaftarWilayahKliring(gm.bic, gm.sAuthCodeBI);
                    }

                    break;

                case "004":
                    if (gm.IsDummy_Parameter)
                    {
                        var filenames = Directory.GetFiles(gm.Dummy_Path_Parameter, "*.004");
                        byte[] file = null;

                        for (int i = 0; i < filenames.Length; i++)
                        {
                            file = File.ReadAllBytes(filenames[i]);
                        }

                        gm.bispk_result = file;
                    }
                    else
                    {
                        gm.bispk_result = Program._wsdl.getDaftarSandiTransaksi(gm.bic, gm.sAuthCodeBI);
                    }
                    break;

                case "005":
                    if (gm.IsDummy_Parameter)
                    {
                        var filenames = Directory.GetFiles(gm.Dummy_Path_Parameter, "*.005");
                        byte[] file = null;

                        for (int i = 0; i < filenames.Length; i++)
                        {
                            file = File.ReadAllBytes(filenames[i]);
                        }

                        gm.bispk_result = file;
                    }
                    else
                    {
                        gm.bispk_result = Program._wsdl.getDaftarAlasanPenolakan(gm.bic, gm.sAuthCodeBI);
                    }
                    break;

                case "006":
                    if (gm.IsDummy_Parameter)
                    {
                        var filenames = Directory.GetFiles(gm.Dummy_Path_Parameter, "*.006");
                        byte[] file = null;

                        for (int i = 0; i < filenames.Length; i++)
                        {
                            file = File.ReadAllBytes(filenames[i]);
                        }

                        gm.bispk_result = file;
                    }
                    else
                    {
                        gm.bispk_result = Program._wsdl.getDaftarPropinsi(gm.bic, gm.sAuthCodeBI);
                    }
                    break;

                case "007":
                    if (gm.IsDummy_Parameter)
                    {
                        var filenames = Directory.GetFiles(gm.Dummy_Path_Parameter, "*.007");
                        byte[] file = null;

                        for (int i = 0; i < filenames.Length; i++)
                        {
                            file = File.ReadAllBytes(filenames[i]);
                        }

                        gm.bispk_result = file;
                    }
                    else
                    {
                        gm.bispk_result = Program._wsdl.getDaftarKota(gm.bic, gm.sAuthCodeBI);
                    }

                    break;

                case "008":
                    if (gm.IsDummy_Parameter)
                    {
                        var filenames = Directory.GetFiles(gm.Dummy_Path_Parameter, "*.008");
                        byte[] file = null;

                        for (int i = 0; i < filenames.Length; i++)
                        {
                            file = File.ReadAllBytes(filenames[i]);
                        }

                        gm.bispk_result = file;
                    }
                    else
                    {
                        gm.bispk_result = Program._wsdl.getDaftarWilayahKota(gm.bic, gm.sAuthCodeBI);
                    }
                    break;
            }

            gm.sResult = enkdek.StringToBase64(gm.bispk_result);

            //remark by DSR
            //Program.writeLog("Response from BI ORI          : " + gm.sResult);

            if (gm.IsEncBISPK)
            {
                gm.sResult = enkdek.DekripBISPK(gm.sResult);
            }
            else
            {
                gm.sResult = enkdek.Base64ToString(gm.sResult);
            }

            gm.sResultTo_TCPSocket = enkdek.EnkripAS400(gm.sResult);

            if (!gm.IsEncAS400)
            {
                gm.sResultTo_TCPSocket = enkdek.DekripAS400(gm.sResultTo_TCPSocket);
            }

            Program.writeLog("Response from BI            : " + gm.sResult);


            if (gm.sResult.Trim().Length > 4)
            {
                gm.msg_seq_to_tcp = "0000";
                gm.transcode_to_tcp = gm.trans_code;

                //remark by DSR
                //Program.writeLog("Response                : " + gm.sResultTo_TCPSocket);

                gm.msg_seq_to_tcp = "0000";
                gm.transcode_to_tcp = gm.trans_code;

                gm.err_code_to_tcp = "      ";
                gm.err_desc_to_tcp = "                                                  ";

                //fix length        3        |         3        |       4        |     6           |     50          |      64400
                gm.msg_to_tcp = gm.msg_type_to_tcp + gm.transcode_to_tcp + gm.msg_seq_to_tcp + gm.err_code_to_tcp + gm.err_desc_to_tcp + gm.sResultTo_TCPSocket;

                gm.msg_length_to_tcp = ("000000" + (gm.msg_to_tcp.Length + 6)).Right(6);

                //fix length        6          
                gm.msg_to_tcp = gm.msg_length_to_tcp + gm.msg_to_tcp;
            }
            else
            {
                gm.msg_seq_to_tcp = "0000";
                gm.transcode_to_tcp = gm.trans_code;

                //remark by DSR
                //Program.writeLog("Response                : " + gm.sResultTo_TCPSocket);

                gm.msg_seq_to_tcp = "0000";
                gm.transcode_to_tcp = gm.trans_code;

                gm.err_code_to_tcp = enkdek.DekripAS400(gm.sResultTo_TCPSocket);
                gm.err_desc_to_tcp = "                                                  ";

                //fix length        3        |         3        |       4        |     6
                gm.msg_to_tcp = gm.msg_type_to_tcp + gm.transcode_to_tcp + gm.msg_seq_to_tcp + gm.err_code_to_tcp;

                gm.msg_length_to_tcp = ("000000" + (gm.msg_to_tcp.Length + 6)).Right(6);

                //fix length    6              |    16
                gm.msg_to_tcp = gm.msg_length_to_tcp + gm.msg_to_tcp;
            }

            //reply = gm.msg_to_tcp;

            if (gm.sResult.Trim().Length > mp.GetMaxMsgLength.MaxMsgLength)
            {
                switch (gm.trans_code)
                {
                    case "002":

                        //recall setting
                        //MessageProcessing mp = new MessageProcessing();
                        //iThreadSleep = mp.GetThreadSleep.ThreadSleep;

                        //iRecSize_getDaftarPesertaWilayah = mp.GetDaftarPesertaWilayah_RecSize.RecordSize_getDaftarPesertaWilayah;
                        //iRecCount_getDaftarPesertaWilayah = mp.GetDaftarPesertaWilayah_RecCount.RecordCount_getDaftarPesertaWilayah;

                        List<string> listData = new List<string>();
                        //listData = mp.GetPartialData(gm.sResult, iRecLength_getDaftarPesertaWilayah, iRecSize_getDaftarPesertaWilayah);
                        listData = mp.GetPartialData(gm.sResult, gm.RecordCount_getDaftarPesertaWilayah);

                        int index = 1;
                        foreach (var list in listData)
                        {
                            if (index == listData.ToArray().Length)
                            {
                                gm.msg_seq_to_tcp = "9999";
                            }
                            else
                            {
                                gm.msg_seq_to_tcp = ("0000" + index).Right(4);
                            }

                            //fix length        3        |         3        |       4        |     6           |     50          |      64400
                            //gm.msg_to_tcp = gm.msg_type_to_tcp + gm.transcode_to_tcp + gm.msg_seq_to_tcp + gm.err_code_to_tcp + gm.err_desc_to_tcp + enkdek.EnkripAS400(list.Replace("#", ""));
                            gm.msg_to_tcp = gm.msg_type_to_tcp + gm.transcode_to_tcp + gm.msg_seq_to_tcp + gm.err_code_to_tcp + gm.err_desc_to_tcp + enkdek.EnkripAS400(list);

                            gm.msg_length_to_tcp = ("000000" + (gm.msg_to_tcp.Length + 6)).Right(6);

                            //fix length        6          
                            gm.msg_to_tcp = gm.msg_length_to_tcp + gm.msg_to_tcp;

                            //reply = gm.msg_to_tcp;

                            //return gm.msg_to_tcp;
                            Program.sendReply(gm.msg_to_tcp);

                            index++;

                            Thread.Sleep(gm.ThreadSleep);
                        }

                        break;
                }
            }
            else
            {
                Program.sendReply(gm.msg_to_tcp);
            }
            return gm.msg_to_tcp;
        }
    }
}
